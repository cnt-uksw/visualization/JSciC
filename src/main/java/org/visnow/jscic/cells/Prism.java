/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.cells;

/**
 * Prism cell.
 *
 * @author Krzysztof S. Nowinski
 *
 * University of Warsaw, ICM
 */
public class Prism extends Cell
{

    private static final long serialVersionUID = -741349166482276656L;

    /**
     * Constructor of a prism.
     *
     * <pre>
     *  i3----------i5
     *  |\         /|
     *  |  \     /  |
     *  |    \ /    |
     *  |     i4    |
     *  |     |     |
     *  i0- - | - - i2
     *   \    |    /
     *     \  |  /        type 0 i1-i5 diagonal
     *       \|/          type 1 i2-i4 diagonal
     *        i1
     *
     * Indices are ordered so that
     *
     * i0&lt;(i1,i2,i3,i4,i5), i1&lt;i2
     * </pre>
     *
     * @param i0          node index
     * @param i1          node index
     * @param i2          node index
     * @param i3          node index
     * @param i4          node index
     * @param i5          node index
     * @param orientation significant if cell is of dim nspace or is a face of a
     *                    cell of dim nspace
     */
    public Prism(int i0, int i1, int i2, int i3, int i4, int i5, byte orientation)
    {
        type = CellType.PRISM;
        vertices = new int[6];
        vertices[0] = i0;
        vertices[1] = i1;
        vertices[2] = i2;
        vertices[3] = i3;
        vertices[4] = i4;
        vertices[5] = i5;
        this.orientation = orientation;
        normalize();
    }

    /**
     * Constructor of a prism.
     *
     * <pre>
     *  i3----------i5
     *  |\         /|
     *  |  \     /  |
     *  |    \ /    |
     *  |     i4    |
     *  |     |     |
     *  i0- - | - - i2
     *   \    |    /
     *     \  |  /        type 0 i1-i5 diagonal
     *       \|/          type 1 i2-i4 diagonal
     *        i1
     *
     * Indices are ordered so that
     *
     * i0&lt;(i1,i2,i3,i4,i5), i1&lt;i2
     * </pre>
     *
     * @param i0  node index
     * @param i1  node index
     * @param i2  node index
     * @param i3  node index
     * @param i4  node index
     * @param i5  node index
     */
    public Prism(int i0, int i1, int i2, int i3, int i4, int i5)
    {
        this(i0, i1, i2, i3, i4, i5, (byte) 1);
    }

    @Override
    public Cell[][] subcells()
    {
        Cell[][] tr = new Cell[4][];
        tr[3] = new Prism[1];
        tr[3][0] = this;
        tr[2] = faces();
        tr[1] = new Segment[9];
        tr[1][0] = new Segment(vertices[0], vertices[1], orientation);
        tr[1][1] = new Segment(vertices[1], vertices[2], orientation);
        tr[1][2] = new Segment(vertices[2], vertices[0], orientation);
        tr[1][3] = new Segment(vertices[3], vertices[4], orientation);
        tr[1][4] = new Segment(vertices[4], vertices[5], orientation);
        tr[1][5] = new Segment(vertices[5], vertices[3], orientation);
        tr[1][6] = new Segment(vertices[0], vertices[3], orientation);
        tr[1][7] = new Segment(vertices[1], vertices[4], orientation);
        tr[1][8] = new Segment(vertices[2], vertices[5], orientation);
        tr[0] = new Point[vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            tr[0][i] = new Point(vertices[i]);
        }
        return tr;
    }

    @Override
    public Cell[] faces()
    {
        return faces(vertices, orientation);
    }

    @Override
    public Cell[] faces(int[] nodes, byte orientation)
    {
        Cell[] faces = {new Triangle(nodes[0], nodes[1], nodes[2], (byte) (1 - orientation)),
                        new Quad(nodes[0], nodes[1], nodes[4], nodes[3], orientation),
                        new Quad(nodes[1], nodes[2], nodes[5], nodes[4], orientation),
                        new Quad(nodes[0], nodes[2], nodes[5], nodes[3], (byte) (1 - orientation)),
                        new Triangle(nodes[3], nodes[4], nodes[5], orientation)};
        return faces;
    }

    @Override
    public Cell[] triangulation()
    {
        Tetra[] subdiv = new Tetra[3];
        subdiv[0] = new Tetra(vertices[0], vertices[3], vertices[4], vertices[5], orientation);
        int i = 1, t = 0;
        for (int j = 2; j < 6; j++) {
            if (vertices[j] < vertices[i] && j != 3) {
                i = j;
            }
        }
        if (i == 1 || i == 5) {
            subdiv[1] = new Tetra(vertices[0], vertices[1], vertices[2], vertices[5], orientation);
            subdiv[2] = new Tetra(vertices[0], vertices[1], vertices[5], vertices[4], orientation);
        } else {
            subdiv[1] = new Tetra(vertices[0], vertices[1], vertices[2], vertices[4], orientation);
            subdiv[2] = new Tetra(vertices[0], vertices[2], vertices[5], vertices[4], orientation);
        }
        return subdiv;
    }

    @Override
    public int[][] triangulationVertices()
    {
        return triangulationVertices(vertices);
    }

    /**
     * Generates the array of tetrahedra vertices from the array of prism
     * vertices.
     *
     * @param vertices array of vertices
     *
     * @return array of tetrahedra vertices
     */
    public static int[][] triangulationVertices(int[] vertices)
    {
        int[][] subdiv = new int[3][];
        subdiv[0] = new int[]{vertices[0], vertices[3], vertices[4], vertices[5]};
        int i = 1, t = 0;
        for (int j = 2; j < 6; j++) {
            if (vertices[j] < vertices[i] && j != 3) {
                i = j;
            }
        }
        if (i == 1 || i == 5) {
            subdiv[1] = new int[]{vertices[0], vertices[1], vertices[2], vertices[5]};
            subdiv[2] = new int[]{vertices[0], vertices[1], vertices[5], vertices[4]};
        } else {
            subdiv[1] = new int[]{vertices[0], vertices[1], vertices[2], vertices[4]};
            subdiv[2] = new int[]{vertices[0], vertices[2], vertices[5], vertices[4]};
        }
        return subdiv;
    }

    /**
     * Generates the array of triangles in boundary triangulation of the
     * prism.
     *
     * @param vertices array of vertices
     *
     * @return array of triangles in boundary triangulation
     */
    public static int[][] triangulationIndices(int[] vertices)
    {
        int[][] subdiv = new int[3][];
        subdiv[0] = new int[]{0, 3, 4, 5};
        int i = 1, t = 0;
        for (int j = 2; j < 6; j++) {
            if (vertices[j] < vertices[i] && j != 3) {
                i = j;
            }
        }
        if (i == 1 || i == 5) {
            subdiv[1] = new int[]{0, 1, 2, 5};
            subdiv[2] = new int[]{0, 1, 5, 4};
        } else {
            subdiv[1] = new int[]{0, 1, 2, 4};
            subdiv[2] = new int[]{0, 2, 5, 4};
        }
        return subdiv;
    }

    @Override
    public final int[] normalize()
    {
        if (!normalize(vertices)) {
            orientation = (byte) (1 - orientation);
        }
        return vertices;
    }

    /**
     * A convenience function that reorders array of vertices to increasing order (as
     * long as possible).
     *
     * @param vertices vertices
     *
     * @return true if orientation has been preserved, false otherwise
     */
    public static boolean normalize(int[] vertices)
    {
        int i = 0, t = 0;
        for (int j = 1; j < 6; j++) {
            if (vertices[j] < vertices[i]) {
                i = j;
            }
        }
        switch (i) {
            case 0:
                break;
            case 1:
                t = vertices[0];
                vertices[0] = vertices[1];
                vertices[1] = vertices[2];
                vertices[2] = t;
                t = vertices[3];
                vertices[3] = vertices[4];
                vertices[4] = vertices[5];
                vertices[5] = t;
                break;
            case 2:
                t = vertices[0];
                vertices[0] = vertices[2];
                vertices[2] = vertices[1];
                vertices[1] = t;
                t = vertices[3];
                vertices[3] = vertices[5];
                vertices[5] = vertices[4];
                vertices[4] = t;
                break;
            case 3:
                t = vertices[0];
                vertices[0] = vertices[3];
                vertices[3] = t;
                t = vertices[1];
                vertices[1] = vertices[5];
                vertices[5] = t;
                t = vertices[2];
                vertices[2] = vertices[4];
                vertices[4] = t;
                break;
            case 4:
                t = vertices[0];
                vertices[0] = vertices[4];
                vertices[4] = t;
                t = vertices[1];
                vertices[1] = vertices[3];
                vertices[3] = t;
                t = vertices[2];
                vertices[2] = vertices[5];
                vertices[5] = t;
                break;
            case 5:
                t = vertices[0];
                vertices[0] = vertices[5];
                vertices[5] = t;
                t = vertices[1];
                vertices[1] = vertices[4];
                vertices[4] = t;
                t = vertices[2];
                vertices[2] = vertices[3];
                vertices[3] = t;
                break;
            default:
                throw new IllegalArgumentException("Invalid index " + i);
        }
        if (vertices[1] > vertices[2]) {
            t = vertices[2];
            vertices[2] = vertices[1];
            vertices[1] = t;
            t = vertices[5];
            vertices[5] = vertices[4];
            vertices[4] = t;
            return false;
        }
        return true;
    }

    @Override
    public byte compare(int[] v)
    {
        if (v.length != 6) {
            return 0;
        }
        return compare(new Prism(v[0], v[1], v[2], v[3], v[4], v[5]));
    }

}
