/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.cells;

/**
 * Segment cell.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Segment extends Cell
{

    private static final long serialVersionUID = -1541346050544363672L;

    /**
     * Constructor of a segment.
     *
     * @param i0          node index
     * @param i1          node index
     * @param orientation significant if cell is of dim nspace or is a face of a cell of dim nspace
     */
    public Segment(int i0, int i1, byte orientation)
    {
        type = CellType.SEGMENT;
        vertices = new int[2];
        vertices[0] = i0;
        vertices[1] = i1;
        this.orientation = orientation;
        normalize();
    }

    /**
     * Constructor of a segment.
     *
     * @param i0  node index
     * @param i1  node index
     */
    public Segment(int i0, int i1)
    {
        this(i0, i1, (byte) 1);
    }

    @Override
    public Cell[][] subcells()
    {
        Cell[][] tr = new Cell[2][];
        tr[1] = new Segment[1];
        tr[1][0] = this;
        tr[0] = new Point[vertices.length];
        for (int i = 0; i < vertices.length; i++)
            tr[0][i] = new Point(vertices[i]);
        return tr;
    }

    @Override
    public Cell[] triangulation()
    {
        Segment[] subdiv = {this};
        return subdiv;
    }

    @Override
    public int[][] triangulationVertices()
    {
        return new int[][]{vertices};
    }

    /**
     * Generates the array of tetrahedra vertices from the array of point
     * vertices.
     *
     * @param vertices array of vertices
     *
     * @return array of tetrahedra vertices
     */
    public static int[][] triangulationVertices(int[] vertices)
    {
        return new int[][]{vertices};
    }

    @Override
    public Cell[] faces()
    {
        return faces(vertices, orientation);
    }

    @Override
    public Cell[] faces(int[] nodes, byte orientation)
    {
        Point[] faces = new Point[2];
        faces[0] = new Point(nodes[0], (byte) 0);
        faces[1] = new Point(nodes[1], (byte) 1);
        return faces;
    }

    @Override
    public final int[] normalize()
    {
        if (!normalize(vertices))
            orientation = (byte) (1 - orientation);
        return vertices;
    }

    /**
     * A convenience function that reorders array of vertices to increasing
     * order (as long as possible).
     *
     * @param vertices vertices
     *
     * @return always true
     */
    public static boolean normalize(int[] vertices)
    {
        if (vertices[0] > vertices[1]) {
            int t = vertices[0];
            vertices[0] = vertices[1];
            vertices[1] = t;
            return false;
        }
        return true;
    }

    @Override
    public byte compare(int[] v)
    {
        if (v.length == 1)
            return 0;
        else if (v[0] != vertices[0])
            return 0;
        else
            return 1;
    }

}
