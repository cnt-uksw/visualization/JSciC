/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.dataarrays;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.TimeData;
import org.visnow.jlargearrays.ConcurrencyUtils;
import static org.visnow.jscic.utils.ConvertUtils.*;
import org.visnow.jscic.utils.EngineeringFormattingUtils;
import org.visnow.jscic.utils.LargeArrayMath;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jscic.dataarrays.DataArraySchema.DataOutsideRangeAction;
import static org.visnow.jscic.utils.FloatingPointUtils.*;
import static org.visnow.jscic.utils.DataArrayStatistics.histogram;

/**
 *
 * The base class for all data arrays used in Field objects. Holds data array by itself and additional information necessary for proper interpretation of data.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
abstract public class DataArray implements java.io.Serializable
{

    public static final int MAX_STRING_LENGTH = 100;
    public static final int MAX_OBJECT_SIZE = 1024; //1KB
    private static final long serialVersionUID = -2499030606888564635L;
    protected DataArraySchema schema;
    protected long timestamp;
    protected TimeData timeData;
    protected boolean hasParent = false;
    protected int previousTimeMaskHashCode = 0;
    protected boolean previousTimeMaskWasNull = true;

    /**
     * Creates a new instance of DataArray.
     *
     * @param schema DataArray schema, this reference is used internally (the array is not cloned).
     */
    protected DataArray(DataArraySchema schema)
    {
        if (schema == null) {
            throw new IllegalArgumentException("schema cannot be null.");
        }
        this.schema = schema;
        this.schema.setStatisticsComputed(false);
        this.timestamp = System.nanoTime();
    }

    /**
     * Creates a new instance of scalar (veclen = 1) constant DataArray.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param constant  if true, then a scalar constant DataArray is created
     */
    protected DataArray(DataArrayType type, long nelements, boolean constant)
    {
        this(type, nelements, 1, constant, "Data", "", null);
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param veclen    vector length
     */
    protected DataArray(DataArrayType type, long nelements, int veclen)
    {
        this(type, nelements, veclen, false, "Data", "", null);
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param veclen    vector length
     * @param constant  if true, then a constant DataArray is created
     * @param name      name of DataArray
     * @param units     units
     * @param userData  user data, this reference is used internally (the array is not cloned)
     */
    protected DataArray(DataArrayType type, long nelements, int veclen, boolean constant, String name, String units, String[] userData)
    {
        schema = new DataArraySchema(name, units, userData, type, nelements, veclen, constant);
        this.timestamp = System.nanoTime();
    }

    @Override
    public String toString()
    {
        return schema.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof DataArray)) {
            return false;
        }
        DataArray da = (DataArray) o;
        return this.schema.equals(da.schema) && this.timeData.equals(da.timeData);
    }

    @Override
    public int hashCode()
    {
        return hashCode(1f);
    }

    /**
     * Generates an approximate hash code using a quality argument. For quality equal to 1 this method is equivalent to hashCode().
     *
     * @param quality a number between 0 and 1.
     *
     * @return an approximate hash code
     */
    public int hashCode(float quality)
    {
        if (quality < 0 || quality > 1) {
            throw new IllegalArgumentException("The quality argument should be between 0 and 1");
        }
        int fprint = 5;
        fprint = 71 * fprint + Objects.hashCode(this.schema);
        if (quality > 0 && timeData != null) {
            fprint = 71 * fprint + timeData.hashCode(quality);
        }
        return fprint;
    }

    /**
     * Returns a description of the DataArray.
     *
     * @return description of the DataArray
     */
    public String description()
    {
        return description(false);
    }

    /**
     * Returns a description of the DataArray.
     *
     * @param debug if true, then the more verbose description is returned
     *
     * @return description of the DataArray
     */
    public String description(boolean debug)
    {
        StringBuilder s = new StringBuilder();
        s.append("<TR valign='bottom' align='right'><TD align = 'left'>").append(schema.getName()).append("</TD><TD>").append(schema.getVectorLength()).append("</TD><TD>");
        switch (schema.getType()) {
            case FIELD_DATA_LOGIC:
                s.append(" logic");
                break;
            case FIELD_DATA_BYTE:
                s.append(" byte");
                break;
            case FIELD_DATA_SHORT:
                s.append(" short");
                break;
            case FIELD_DATA_INT:
                s.append(" int");
                break;
            case FIELD_DATA_LONG:
                s.append(" long");
                break;
            case FIELD_DATA_FLOAT:
                s.append(" float");
                break;
            case FIELD_DATA_DOUBLE:
                s.append(" double");
                break;
            case FIELD_DATA_COMPLEX:
                s.append(" complex");
                break;
            case FIELD_DATA_STRING:
                s.append(" string");
                break;
            case FIELD_DATA_OBJECT:
                ObjectDataArray oda = (ObjectDataArray) this;
                if (oda.getRawArray().get(0) != null) {
                    s.append(oda.getRawArray().get(0).getClass().getName());
                } else {
                    s.append(" object");
                }
                break;
            default:
                s.append(" unknown");
                break;
        }

        s.append("</TD><TD>");
        s.append(getNFrames());
        s.append("</TD><TD>");

        switch (schema.getType()) {
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_BYTE:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_STRING:
                s.append((int) getMinValue());
                s.append("</TD><TD>");
                s.append((int) getMaxValue());
                s.append("</TD><TD>");
                break;
            default:
                String[] minMax = EngineeringFormattingUtils.formatInContextHtml(new double[]{getMinValue(), getMaxValue()});
                s.append(minMax[0]);
                s.append("</TD><TD>");
                s.append(minMax[1]);
                s.append("</TD><TD>");
        }

        String[] physMinMax = EngineeringFormattingUtils.formatInContextHtml(new double[]{getPhysMinValue(), getPhysMaxValue()});
        s.append(physMinMax[0]);
        s.append("</TD><TD>");
        s.append(physMinMax[1]);
        s.append("</TD>");
        if (!debug) {
            s.append("<TD>");
            if (isUnitless()) {
                s.append("1");
            } else {
                s.append(getUnit());
            }
            s.append("</TD>");
        }
        if (debug) {
            s.append("<TD>");
            switch (schema.getType()) {
                case FIELD_DATA_LOGIC:
                case FIELD_DATA_BYTE:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_STRING:
                    s.append((int) getPreferredMinValue());
                    s.append("</TD><TD>");
                    s.append((int) getPreferredMaxValue());
                    s.append("</TD><TD>");
                    break;
                default:
                    String[] prefMinMax = EngineeringFormattingUtils.formatInContextHtml(new double[]{getPreferredMinValue(), getPreferredMaxValue()});
                    s.append(prefMinMax[0]);
                    s.append("</TD><TD>");
                    s.append(prefMinMax[1]);
                    s.append("</TD><TD>");
            }

            String[] prefPhysMinMax = EngineeringFormattingUtils.formatInContextHtml(new double[]{getPreferredPhysMinValue(), getPreferredPhysMaxValue()});
            s.append(prefPhysMinMax[0]);
            s.append("</TD><TD>");
            s.append(prefPhysMinMax[1]);
            s.append("</TD><TD>");
            if (isUnitless()) {
                s.append("1");
            } else {
                s.append(getUnit());
            }
            s.append("</TD><TD>");
            if (getUserData() != null) {
                s.append("[");
                for (int i = 0; i < getUserData().length; i++) {
                    s.append(getUserData(i));
                    if (i < getUserData().length - 1) {
                        s.append(",");
                    }
                }
                s.append("]");
            } else {
                s.append("none");
            }
            s.append("</TD>");
        }

        s.append("</TR>");

        return s.toString();
    }

    /**
     * Creates a new instance of constant DataArray. Vector length is always equal 1.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param value     constant value
     *
     * @return new instance of DataArray
     */
    public static DataArray createConstant(DataArrayType type, long nelements, Object value)
    {
        return createConstant(type, nelements, value, "constantArray", "", null);
    }

    /**
     * Creates a new instance of constant DataArray. Vector length is always equal 1.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param value     constant value
     * @param name      name of DataArray
     *
     * @return new instance of DataArray
     */
    public static DataArray createConstant(DataArrayType type, long nelements, Object value, String name)
    {
        return createConstant(type, nelements, value, name, "", null);
    }

    /**
     * Creates a new instance of constant DataArray. Vector length is always equal 1.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param value     constant value
     * @param name      name of DataArray
     * @param units     units
     * @param userData  user data, this reference is used internally (the array is not cloned)
     *
     * @return new instance of DataArray
     */
    public static DataArray createConstant(DataArrayType type, long nelements, Object value, String name, String units, String[] userData)
    {
        LargeArray data = LargeArrayUtils.createConstant(type.toLargeArrayType(), nelements, value);
        DataArraySchema schema = new DataArraySchema(name, type, nelements, 1, true);
        schema.setUnit(units);
        schema.setUserData(userData);

        switch (type) {
            case FIELD_DATA_BYTE:
                return new ByteDataArray((UnsignedByteLargeArray) data, schema);
            case FIELD_DATA_SHORT:
                return new ShortDataArray((ShortLargeArray) data, schema);
            case FIELD_DATA_INT:
                return new IntDataArray((IntLargeArray) data, schema);
            case FIELD_DATA_LONG:
                return new LongDataArray((LongLargeArray) data, schema);
            case FIELD_DATA_FLOAT:
                return new FloatDataArray((FloatLargeArray) data, schema);
            case FIELD_DATA_DOUBLE:
                return new DoubleDataArray((DoubleLargeArray) data, schema);
            case FIELD_DATA_COMPLEX:
                return new ComplexDataArray((ComplexFloatLargeArray) data, schema);
            case FIELD_DATA_LOGIC:
                return new LogicDataArray((LogicLargeArray) data, schema);
            case FIELD_DATA_STRING:
                return new StringDataArray((StringLargeArray) data, schema);
            case FIELD_DATA_OBJECT:
                return new ObjectDataArray((ObjectLargeArray) data, schema);
            default:
                throw new IllegalArgumentException("Data of type: " + type + " is not supported!");
        }
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param veclen    vector length
     *
     * @return new instance of DataArray
     */
    public static DataArray create(DataArrayType type, long nelements, int veclen)
    {
        switch (type) {
            case FIELD_DATA_BYTE:
                return new ByteDataArray(nelements, veclen);
            case FIELD_DATA_SHORT:
                return new ShortDataArray(nelements, veclen);
            case FIELD_DATA_INT:
                return new IntDataArray(nelements, veclen);
            case FIELD_DATA_LONG:
                return new LongDataArray(nelements, veclen);
            case FIELD_DATA_FLOAT:
                return new FloatDataArray(nelements, veclen);
            case FIELD_DATA_DOUBLE:
                return new DoubleDataArray(nelements, veclen);
            case FIELD_DATA_COMPLEX:
                return new ComplexDataArray(nelements, veclen);
            case FIELD_DATA_LOGIC:
                return new LogicDataArray(nelements, veclen);
            case FIELD_DATA_STRING:
                return new StringDataArray(nelements, veclen);
            case FIELD_DATA_OBJECT:
                return new ObjectDataArray(nelements, veclen);
            default:
                throw new IllegalArgumentException("Data of type: " + type + " is not supported!");
        }
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param schema DataArray schema, this reference is used internally (the array is not cloned)
     *
     * @return new instance of DataArray
     */
    public static DataArray create(DataArraySchema schema)
    {
        switch (schema.getType()) {
            case FIELD_DATA_BYTE:
                return new ByteDataArray(schema);
            case FIELD_DATA_SHORT:
                return new ShortDataArray(schema);
            case FIELD_DATA_INT:
                return new IntDataArray(schema);
            case FIELD_DATA_LONG:
                return new LongDataArray(schema);
            case FIELD_DATA_FLOAT:
                return new FloatDataArray(schema);
            case FIELD_DATA_DOUBLE:
                return new DoubleDataArray(schema);
            case FIELD_DATA_COMPLEX:
                return new ComplexDataArray(schema);
            case FIELD_DATA_LOGIC:
                return new LogicDataArray(schema);
            case FIELD_DATA_STRING:
                return new StringDataArray(schema);
            case FIELD_DATA_OBJECT:
                return new ObjectDataArray(schema);
            default:
                throw new IllegalArgumentException("Data of type: " + schema.getType() + " is not supported!");
        }
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param type      type of DataArray
     * @param nelements number of elements
     * @param veclen    vector length
     * @param name      name of DataArray
     * @param units     units
     * @param userData  user data, this reference is used internally (the array is not cloned)
     *
     * @return new instance of DataArray
     */
    public static DataArray create(DataArrayType type, long nelements, int veclen, String name, String units, String[] userData)
    {
        switch (type) {
            case FIELD_DATA_BYTE:
                return new ByteDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_SHORT:
                return new ShortDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_INT:
                return new IntDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_LONG:
                return new LongDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_FLOAT:
                return new FloatDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_DOUBLE:
                return new DoubleDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_COMPLEX:
                return new ComplexDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_LOGIC:
                return new LogicDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_STRING:
                return new StringDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            case FIELD_DATA_OBJECT:
                return new ObjectDataArray(new DataArraySchema(name, units, userData, type, nelements, veclen, false));
            default:
                throw new IllegalArgumentException("Data of type: " + type + " is not supported!");
        }
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param data     array of elements, this reference is used internally (the array is not cloned)
     * @param veclen   vector length
     * @param name     name of DataArray
     * @param units    units
     * @param userData user data, this reference is used internally (the array is not cloned)
     *
     * @return new instance of DataArray
     */
    public static DataArray create(Object data, int veclen, String name, String units, String[] userData)
    {
        if (data == null) {
            throw new IllegalArgumentException("data cannot be null");
        }

        Class dataClass = data.getClass();
        Class componentClass = dataClass.getComponentType();

        if (dataClass == LogicLargeArray.class || dataClass == UnsignedByteLargeArray.class || dataClass == ShortLargeArray.class ||
            dataClass == IntLargeArray.class || dataClass == LongLargeArray.class || dataClass == FloatLargeArray.class || dataClass == DoubleLargeArray.class ||
            dataClass == ComplexFloatLargeArray.class || dataClass == StringLargeArray.class || dataClass == ObjectLargeArray.class) {
            return create((LargeArray) data, veclen, name, units, userData);
        } else if (dataClass == TimeData.class) {
            return create((TimeData) data, veclen, name, units, userData);
        } else if (componentClass == Boolean.TYPE) {
            return new LogicDataArray(new LogicLargeArray((boolean[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_LOGIC, ((boolean[]) data).length / veclen, veclen, false));
        } else if (componentClass == Byte.TYPE) {
            return new ByteDataArray(new UnsignedByteLargeArray((byte[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_BYTE, ((byte[]) data).length / veclen, veclen, false));
        } else if (componentClass == Short.TYPE) {
            return new ShortDataArray(new ShortLargeArray((short[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_SHORT, ((short[]) data).length / veclen, veclen, false));
        } else if (componentClass == Integer.TYPE) {
            return new IntDataArray(new IntLargeArray((int[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_INT, ((int[]) data).length / veclen, veclen, false));
        } else if (componentClass == Long.TYPE) {
            return new LongDataArray(new LongLargeArray((long[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_LONG, ((long[]) data).length / veclen, veclen, false));
        } else if (componentClass == Float.TYPE) {
            return new FloatDataArray(new FloatLargeArray((float[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_FLOAT, ((float[]) data).length / veclen, veclen, false));
        } else if (componentClass == Double.TYPE) {
            return new DoubleDataArray(new DoubleLargeArray((double[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_DOUBLE, ((double[]) data).length / veclen, veclen, false));
        } else if (componentClass == String.class) {
            return new StringDataArray(new StringLargeArray((String[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_STRING, ((String[]) data).length / veclen, veclen, false));
        } else if (componentClass == DataObjectInterface.class || DataObjectInterface.class.isAssignableFrom(componentClass)) {
            return new ObjectDataArray(new ObjectLargeArray((DataObjectInterface[]) data), new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_OBJECT, ((DataObjectInterface[]) data).length / veclen, veclen, false));
        } else {
            throw new IllegalArgumentException("Data of type: " + dataClass + " is not supported!");
        }
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param data     array of elements, this reference is used internally (the array is not cloned)
     * @param veclen   vector length
     * @param name     name of DataArray
     * @param units    units
     * @param userData user data, this reference is used internally (the array is not cloned)
     *
     * @return new instance of DataArray
     */
    public static DataArray create(TimeData data, int veclen, String name, String units, String[] userData)
    {

        if (data == null) {
            throw new IllegalArgumentException("data cannot be null");
        }

        boolean constant = data.getNSteps() == 1 ? data.getValues().get(0).isConstant() : false;

        switch (data.getType()) {
            case FIELD_DATA_LOGIC:
                return new LogicDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_LOGIC, data.length() / veclen, veclen, constant));
            case FIELD_DATA_BYTE:
                return new ByteDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_BYTE, data.length() / veclen, veclen, constant));
            case FIELD_DATA_SHORT:
                return new ShortDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_SHORT, data.length() / veclen, veclen, constant));
            case FIELD_DATA_INT:
                return new IntDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_INT, data.length() / veclen, veclen, constant));
            case FIELD_DATA_LONG:
                return new LongDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_LONG, data.length() / veclen, veclen, constant));
            case FIELD_DATA_FLOAT:
                return new FloatDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_FLOAT, data.length() / veclen, veclen, constant));
            case FIELD_DATA_DOUBLE:
                return new DoubleDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_DOUBLE, data.length() / veclen, veclen, constant));
            case FIELD_DATA_COMPLEX:
                return new ComplexDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_COMPLEX, data.length() / veclen, veclen, constant));
            case FIELD_DATA_STRING:
                return new StringDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_STRING, data.length() / veclen, veclen, constant));
            case FIELD_DATA_OBJECT:
                return new ObjectDataArray(data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_OBJECT, data.length() / veclen, veclen, constant));
            default:
                throw new IllegalArgumentException("Data of type: " + data.getType() + " is not supported!");
        }
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param data     array of elements, this reference is used internally (the array is not cloned)
     * @param veclen   vector length
     * @param name     name of DataArray
     * @param units    units
     * @param userData user data, this reference is used internally (the array is not cloned)
     *
     * @return new instance of DataArray
     */
    public static DataArray create(LargeArray data, int veclen, String name, String units, String[] userData)
    {

        if (data == null) {
            throw new IllegalArgumentException("data cannot be null");
        }

        switch (data.getType()) {
            case LOGIC:
                return new LogicDataArray((LogicLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_LOGIC, data.length() / veclen, veclen, data.isConstant()));
            case UNSIGNED_BYTE:
                return new ByteDataArray((UnsignedByteLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_BYTE, data.length() / veclen, veclen, data.isConstant()));
            case SHORT:
                return new ShortDataArray((ShortLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_SHORT, data.length() / veclen, veclen, data.isConstant()));
            case INT:
                return new IntDataArray((IntLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_INT, data.length() / veclen, veclen, data.isConstant()));
            case LONG:
                return new LongDataArray((LongLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_LONG, data.length() / veclen, veclen, data.isConstant()));
            case FLOAT:
                return new FloatDataArray((FloatLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_FLOAT, data.length() / veclen, veclen, data.isConstant()));
            case DOUBLE:
                return new DoubleDataArray((DoubleLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_DOUBLE, data.length() / veclen, veclen, data.isConstant()));
            case COMPLEX_FLOAT:
                return new ComplexDataArray((ComplexFloatLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_COMPLEX, data.length() / veclen, veclen, data.isConstant()));
            case STRING:
                return new StringDataArray((StringLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_STRING, data.length() / veclen, veclen, data.isConstant()));
            case OBJECT:
                return new ObjectDataArray((ObjectLargeArray) data, new DataArraySchema(name, units, userData, DataArrayType.FIELD_DATA_OBJECT, data.length() / veclen, veclen, data.isConstant()));
            default:
                throw new IllegalArgumentException("Data of type: " + data.getType() + " is not supported!");
        }
    }

    /**
     * Creates a new instance of DataArray.
     *
     * @param data   array of elements, this reference is used internally (the array is not cloned)
     * @param veclen vector length
     * @param name   name of DataArray
     *
     * @return new instance of DataArray
     */
    public static DataArray create(Object data, int veclen, String name)
    {
        return create(data, veclen, name, "", null);
    }

    /**
     * Adds the specified array at timestep time. The input array is not cloned.
     *
     * @param d    array of elements
     * @param time timestep
     * @param recomputeStats hints if data array statistics will be recomputed (if e.g. 1000 timesteps are added, ca .5M useless statistics computations can occur)
     */
    public void addRawArray(LargeArray d, float time, boolean recomputeStats)
    {

        if (d == null) {
            throw new IllegalArgumentException("d cannot be null");
        }

        if (d.getType() == getType().toLargeArrayType()) {
            timeData.setValue(d, time);
            timeData.setCurrentTime(time);
            if (recomputeStats)
                recomputeStatistics();
            timestamp = System.nanoTime();
        } else {
            throw new IllegalArgumentException("Invalid array type");
        }
    }

    /**
     * Adds the specified array at timestep time. The input array is not cloned.
     * This is a convenience/backward compatibility method)
     *
     * @param d    array of elements
     * @param time timestep
     */
    public void addRawArray(LargeArray d, float time)
    {
        addRawArray(d, time, true);
    }

    /**
     * Returns the number of timesteps.
     *
     * @return number of timesteps
     */
    public int getNFrames()
    {
        return timeData.getNSteps();
    }

    /**
     * Returns a shallow copy of this instance.
     *
     * @return a shallow copy of this instance
     */
    abstract public DataArray cloneShallow();

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    abstract public DataArray cloneDeep();

    /**
     * Returns a shallow copy of the input list.
     *
     * @param dataArrays list to be cloned
     *
     * @return a shallow copy of this instance
     */
    public static ArrayList<DataArray> cloneShallow(ArrayList<DataArray> dataArrays)
    {
        ArrayList<DataArray> clonedArrays = new ArrayList<>();
        for (DataArray dataArray : dataArrays) {
            clonedArrays.add(dataArray.cloneShallow());
        }
        return clonedArrays;
    }

    /**
     * Returns a deep copy of the input list (the elements themselves are copied).
     *
     * @param dataArrays list to be cloned
     *
     * @return a deep copy of this instance
     */
    public static ArrayList<DataArray> cloneDeep(ArrayList<DataArray> dataArrays)
    {
        ArrayList<DataArray> clonedArrays = new ArrayList<>();
        for (DataArray dataArray : dataArrays) {
            clonedArrays.add(dataArray.cloneDeep());
        }
        return clonedArrays;
    }

    /**
     * Returns the type.
     *
     * @return type of the DataArray
     */
    public DataArrayType getType()
    {
        return schema.getType();
    }

    /**
     *
     * Returns the dimensions of each matrix element of the DataArray.
     *
     * @return	the dimensions of each matrix element of the DataArray
     */
    public int[] getMatrixDims()
    {
        return schema.getMatrixDims();
    }

    /**
     *
     * Returns true if each element of the DataArray is a symmetric matrix, false otherwise,
     *
     * @return	true if each element of the DataArray is a symmetric matrix, false otherwise
     */
    public boolean isSymmetric()
    {
        return schema.isSymmetric();
    }

    /**
     *
     * Sets the matrix dimensions and symmetric flag.
     *
     * @param	matrixDims - dimensions of each element off the DataArray.
     * @param symmetric  - symmetric array indicator checks for parameter compatibility
     *
     * @return itself
     */
    public DataArray setMatrixProperties(int[] matrixDims, boolean symmetric)
    {
        schema.setMatrixProperties(matrixDims, symmetric);
        this.timestamp = System.nanoTime();
        return this;
    }

    /**
     * Returns the preferred minimum value.
     *
     * @return preferred minimum value
     */
    public double getPreferredMinValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getPreferredMinValue();
    }

    /**
     * Returns the preferred maximum value.
     *
     * @return preferred maximum value
     */
    public double getPreferredMaxValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getPreferredMaxValue();
    }

    /**
     * Returns the preferred physical minimum value.
     *
     * @return preferred physical minimum
     */
    public double getPreferredPhysMinValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getPreferredPhysMinValue();
    }

    /**
     * Returns the preferred physical maximum value.
     *
     * @return preferred physical maximum
     */
    public double getPreferredPhysMaxValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getPreferredPhysMaxValue();
    }

    /**
     * Returns a copy of coefficients for linear mapping to physical minimum and maximum values.
     *
     * @return a copy of coefficients for linear mapping to physical minimum and maximum values
     */
    public double[] getPhysicalMappingCoefficients()
    {
        return schema.getPhysicalMappingCoefficients();
    }

    /**
     * Returns the physical maximum value.
     *
     * @return physical maximum
     */
    public double getPhysMaxValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getPhysMaxValue();
    }

    /**
     * Returns the physical minimum value.
     *
     * @return physical maximum
     */
    public double getPhysMinValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getPhysMinValue();
    }

    /**
     * Returns the maximum value.
     *
     * @return maximum value
     */
    public double getMaxValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getMaxValue();
    }

    /**
     * Returns the minimum value.
     *
     * @return minimum value
     */
    public double getMinValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getMinValue();
    }

    /**
     * Returns the mean value.
     *
     * @return mean value
     */
    public double getMeanValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getMeanValue();
    }

    /**
     * Returns the mean of squared values.
     *
     * @return square of the mean value
     */
    public double getMeanSquaredValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getMeanSquaredValue();
    }

    /**
     * Returns standard deviation value.
     *
     * @return standard deviation value
     */
    public double getStandardDeviationValue()
    {
        if (!schema.isStatisticsComputed()) {
            recomputeStatistics();
        }
        return schema.getStandardDeviationValue();
    }

    /**
     * Returns vector length.
     *
     * @return vector length
     */
    public int getVectorLength()
    {
        return schema.getVectorLength();
    }

    /**
     * Returns the number of elements in the array.
     *
     * @return number of elements in the array
     */
    public long getNElements()
    {
        return schema.getNElements();
    }

    /**
     * Returns the name of the array.
     *
     * @return name of the array
     */
    public String getName()
    {
        return schema.getName();
    }

    /**
     * Sets the name of the array.
     *
     * @param name new name of the array
     */
    public void setName(String name)
    {
        schema.setName(name);
        this.timestamp = System.nanoTime();
    }

    /**
     * Sets the name of the array.
     *
     * @param name new name of the array
     *
     * @return itself
     */
    public DataArray name(String name)
    {
        setName(name);
        return this;
    }

    /**
     * Sets the preferred minimum and preferred maximum values.
     *
     * @param preferredMin preferred minimum
     * @param preferredMax preferred maximum
     */
    public void setPreferredRange(double preferredMin, double preferredMax)
    {
        schema.setPreferredRange(preferredMin, preferredMax);
        schema.setDefaultHistogram(null);
        this.timestamp = System.nanoTime();
    }

    /**
     * Sets the preferred minimum and preferred maximum values.
     *
     * @param preferredMin preferred minimum
     * @param preferredMax preferred maximum
     *
     * @return itself
     *
     */
    public DataArray preferredRange(double preferredMin, double preferredMax)
    {
        setPreferredRange(preferredMin, preferredMax);
        return this;
    }

    /**
     * Sets the preferred minimum and maximum values as well as preferred physical minimum and maximum values. Linear mapping coefficients between preferred
     * range and preferred physical range are computed and stored instead of preferredPhysMin and preferredPhysMax values.
     *
     * @param preferredMin     preferred minimum
     * @param preferredMax     preferred maximum
     * @param preferredPhysMin preferred physical minimum
     * @param preferredPhysMax preferred physical maximum
     */
    public void setPreferredRanges(double preferredMin, double preferredMax, double preferredPhysMin, double preferredPhysMax)
    {
        schema.setPreferredRanges(preferredMin, preferredMax, preferredPhysMin, preferredPhysMax);
        schema.setDefaultHistogram(null);
        this.timestamp = System.nanoTime();
    }

    /**
     * Sets the preferred minimum and maximum values as well as preferred physical minimum and maximum values. Linear mapping coefficients between preferred
     * range and preferred physical range are computed and stored instead of preferredPhysMin and preferredPhysMax values.
     *
     * @param preferredMin     preferred minimum
     * @param preferredMax     preferred maximum
     * @param preferredPhysMin preferred physical minimum
     * @param preferredPhysMax preferred physical maximum
     *
     * @return itself
     */
    public DataArray preferredRanges(double preferredMin, double preferredMax, double preferredPhysMin, double preferredPhysMax)
    {
        setPreferredRanges(preferredMin, preferredMax, preferredPhysMin, preferredPhysMax);
        return this;
    }

    /**
     * Returns data outside range action
     *
     * @return data outside range action
     */
    public DataOutsideRangeAction getDataOutsideRangeAction()
    {
        return schema.getDataOutsideRangeAction();
    }

    /**
     * Sets the new value of data outside range action
     *
     * @param dataOutsideRangeAction data outside range action
     */
    public void setDataOutsideRangeAction(DataOutsideRangeAction dataOutsideRangeAction)
    {
        schema.setDataOutsideRangeAction(dataOutsideRangeAction);
    }

    /**
     * processing of data outside of [preferredMin, preferredMax] range: LEAVE - process as data inside range CLAMP - substitute data above preferredMax by
     * preferredMax, data below preferredMin by preferredMin MASK_OUT - process data as if mask = false (ignore)
     *
     * @param dataOutsideRange new value of processing mode
     *
     * @return this array after modification
     */
    public DataArray dataOutsideRange(DataOutsideRangeAction dataOutsideRange)
    {
        schema.setDataOutsideRangeAction(dataOutsideRange);
        return this;
    }

    /**
     * Returns the unit of this DataArray.
     *
     * @return unit
     */
    public String getUnit()
    {
        return schema.getUnit();
    }

    /**
     * Sets the unit of this DataArray.
     *
     * @param unit new unit of this DataArray
     */
    public void setUnit(String unit)
    {
        schema.setUnit(unit);
        this.timestamp = System.nanoTime();
    }

    /**
     * Returns true if this DataArray has a unit, false otherwise.
     *
     * @return true if this DataArray has a unit, false otherwise
     */
    public boolean isUnitless()
    {
        return getUnit() == null || getUnit().isEmpty() || getUnit().equals("1");
    }

    /**
     * Sets the unit of this DataArray.
     *
     * @param unit new unit of this DataArray
     *
     * @return itself
     */
    public DataArray unit(String unit)
    {
        setUnit(unit);
        return this;
    }

    /**
     * Returns true if DataArray type is a numeric type, false otherwise.
     *
     * @return true if DataArray type is a numeric type, false otherwise
     */
    public boolean isNumeric()
    {
        return schema.isNumeric();
    }

    /**
     * Returns true if DataArray is constant, false otherwise.
     *
     * @return true if DataArray is constant, false otherwise
     */
    public boolean isConstant()
    {
        return schema.isConstant();
    }

    /**
     * Returns DataArray schema. This method always returns a reference to the internal DataArraySchema.
     *
     * @return DataArray schema
     */
    public DataArraySchema getSchema()
    {
        return schema;
    }

    /**
     * Returns true if a given DataArray is compatible with this DataArray, false otherwise.
     *
     * @param a DataArray
     *
     * @return true if a given DataArray is compatible with this DataArray, false otherwise
     */
    public boolean isCompatibleWith(DataArray a)
    {
        return schema.isCompatibleWith(a.getSchema());
    }

    /**
     * Returns true if a given DataArray is fully compatible with this DataArray, false otherwise.
     *
     * @param a                   DataArray
     *
     * @param checkComponentNames if true, then the component names are compared
     *
     * @return true if a given DataArray is fully compatible with this DataArray, false otherwise
     */
    public boolean isFullyCompatibleWith(DataArray a, boolean checkComponentNames)
    {
        return schema.isCompatibleWith(a.getSchema(), checkComponentNames) &&
            getTimeData().isTimeCompatibleWith(a.getTimeData());
    }

    /**
     * Recomputes DataArray statistics and updates the schema.
     */
    public void recomputeStatistics()
    {
        recomputeStatistics(null, false);
    }

    /**
     * Recomputes DataArray statistics and updates the schema.
     *
     * @param recomputePreferredMinMax if true, then preferred minimum and maximum values are recomputed.
     */
    public void recomputeStatistics(boolean recomputePreferredMinMax)
    {
        recomputeStatistics(null, recomputePreferredMinMax);
    }

    /**
     * Recomputes DataArray statistics and updates the schema.
     *
     * @param timeMask                 mask
     * @param recomputePreferredMinMax if true, then preferred minimum and maximum values are recomputed.
     */
    public void recomputeStatistics(TimeData timeMask, boolean recomputePreferredMinMax)
    {
        final int vlen = getVectorLength();
        double minv = Double.MAX_VALUE;
        double maxv = -Double.MAX_VALUE;
        double meanv = 0;
        double mean2v = 0;
        long mlength = 0;
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();

        if (timeMask == null || timeMask.getNSteps() <= 0) {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final LargeArray dta = timeData.getValues().get(step);
                if (vlen == 1) {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    double d = dta.getDouble(i);
                                    mean += d;
                                    mean2 += d * d;
                                    mlength++;
                                    if (d < min) {
                                        min = d;
                                    }
                                    if (d > max) {
                                        max = d;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads / vlen;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k * vlen;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k * vlen;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        v += dta.getDouble(i + j) * dta.getDouble(i + j);
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        } else {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final LargeArray dta = timeData.getValues().get(step);
                final LargeArray mask;
                if (timeMask.getNSteps() != timeData.getNSteps()) {
                    mask = timeMask.getValues().get(0);
                } else {
                    mask = timeMask.getValues().get(step);
                }
                if (vlen == 1) {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    if (mask.getByte(i) == 0) {
                                        continue;
                                    }
                                    double d = dta.getDouble(i);
                                    mean += d;
                                    mean2 += d * d;
                                    mlength++;
                                    if (d < min) {
                                        min = d;
                                    }
                                    if (d > max) {
                                        max = d;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads / vlen;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k * vlen;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k * vlen;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {

                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    if (mask.getByte(i / vlen) == 0) {
                                        continue;
                                    }
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        v += dta.getDouble(i + j) * dta.getDouble(i + j);
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }
        meanv /= (double) mlength;
        mean2v /= (double) mlength;
        setStatistics(timeMask, minv, maxv, meanv, mean2v, recomputePreferredMinMax);
    }

    /**
     * Updates the DataArray schema with the new statistics.
     *
     * @param timeMask                 mask
     * @param minv                     minimum value
     * @param maxv                     maximum value
     * @param meanv                    mean value
     * @param mean2v                   mean squared value
     * @param recomputePreferredMinMax if true, then preferred min and preferred max values are recomputed
     */
    protected void setStatistics(TimeData timeMask, double minv, double maxv, double meanv, double mean2v, boolean recomputePreferredMinMax)
    {
        double sdv = 0;
        if (maxv < minv) {
            minv = maxv = meanv = mean2v = sdv = Double.NaN;
        } else {
            if (minv < sqrt(2) * MIN_NUMBER_FLOAT || maxv > sqrt(2) * MAX_NUMBER_FLOAT ||
                meanv < sqrt(2) * MIN_NUMBER_FLOAT || meanv > sqrt(2) * MAX_NUMBER_FLOAT ||
                mean2v < sqrt(2) * MIN_NUMBER_FLOAT || mean2v > sqrt(2) * MAX_NUMBER_FLOAT) {
                if (minv < sqrt(2) * MIN_NUMBER_FLOAT) {
                    minv = sqrt(2) * MIN_NUMBER_FLOAT;
                }
                if (maxv > sqrt(2) * MAX_NUMBER_FLOAT) {
                    maxv = sqrt(2) * MAX_NUMBER_FLOAT;
                }
                if (meanv < sqrt(2) * MIN_NUMBER_FLOAT) {
                    meanv = sqrt(2) * MIN_NUMBER_FLOAT;
                } else if (meanv > sqrt(2) * MAX_NUMBER_FLOAT) {
                    meanv = sqrt(2) * MAX_NUMBER_FLOAT;
                }
                if (mean2v < sqrt(2) * MIN_NUMBER_FLOAT) {
                    mean2v = sqrt(2) * MIN_NUMBER_FLOAT;
                } else if (mean2v > sqrt(2) * MAX_NUMBER_FLOAT) {
                    mean2v = sqrt(2) * MAX_NUMBER_FLOAT;
                }
            }
            sdv = sqrt(max(0, mean2v - meanv * meanv));
            if (sdv < sqrt(2) * MIN_NUMBER_FLOAT) {
                sdv = sqrt(2) * MIN_NUMBER_FLOAT;
            } else if (sdv > sqrt(2) * MAX_NUMBER_FLOAT) {
                sdv = sqrt(2) * MAX_NUMBER_FLOAT;
            }
        }
        schema.setMinMaxValues(minv, maxv);
        schema.setMeanValue(meanv);
        schema.setMeanSquaredValue(mean2v);
        schema.setStandardDeviationValue(sdv);
        if (schema.getPreferredMinValue() == MIN_NUMBER_DOUBLE || schema.getPreferredMaxValue() == MAX_NUMBER_DOUBLE || recomputePreferredMinMax) {
            double preferredMin;
            double preferredMax;
            minv = schema.getMinValue();
            maxv = schema.getMaxValue();
            DataArrayType type = getType();
            switch (type) {
                case FIELD_DATA_LOGIC:
                    preferredMin = 0.0;
                    preferredMax = Math.sqrt(getVectorLength());
                    break;
                case FIELD_DATA_BYTE:
                    preferredMin = 0.0;
                    preferredMax = 255.0 * Math.sqrt(getVectorLength());
                    break;
                default:
                    if (getVectorLength() > 1 || type == DataArrayType.FIELD_DATA_STRING) {
                        preferredMin = 0;
                    } else {
                        preferredMin = minv;
                    }
                    preferredMax = maxv;
                    if (preferredMin == preferredMax) {
                        switch (getType()) {
                            case FIELD_DATA_SHORT:
                            case FIELD_DATA_INT:
                            case FIELD_DATA_LONG:
                                preferredMin--;
                                preferredMax++;
                                break;
                            case FIELD_DATA_STRING:
                                preferredMax++;
                                break;
                            default:
                                if (preferredMin == 0.0) {
                                    preferredMin = -0.1;
                                    preferredMax = 0.1;
                                } else {
                                    preferredMin -= 0.1 * abs(preferredMin);
                                    preferredMax += 0.1 * abs(preferredMax);
                                }
                        }
                    }
            }
            schema.setPreferredRange(preferredMin, preferredMax);
        }
        schema.setDefaultHistogram(null);
        schema.setStatisticsComputed(true);
        this.timestamp = System.nanoTime();
    }

    /**
     * Returns the default histogram of all time steps.
     *
     * @param timeMask mask (can be null)
     *
     * @return the default histogram of all time steps.
     */
    public long[] getDefalutHistogram(TimeData timeMask)
    {
        if (schema.getDefaultHistogram() == null ||
            (previousTimeMaskWasNull == false && timeMask == null) ||
            (previousTimeMaskWasNull == true && timeMask != null) ||
            (timeMask != null && previousTimeMaskHashCode != timeMask.hashCode())) {
            schema.setDefaultHistogram(getHistogram(schema.getPreferredMinValue(), schema.getPreferredMaxValue(), DataArraySchema.DEFAULT_HISTOGRAM_LENGTH, false, timeMask));
        }
        if (timeMask == null) {
            previousTimeMaskWasNull = true;
        } else {
            previousTimeMaskWasNull = false;
            previousTimeMaskHashCode = timeMask.hashCode();
        }
        return schema.getDefaultHistogram();
    }

    /**
     * Returns the histogram of the current time step.
     *
     * @return the histogram of the current time step.
     */
    public long[] getCurrentHistogram()
    {
        return getCurrentHistogram(getPreferredMinValue(), getPreferredMaxValue(), 256, false, null);
    }

    /**
     * Returns the histogram of the current time step. The data outside [min,max) interval are ignored.
     *
     * @param min                 min value
     * @param max                 max value
     * @param nbin                number of bins
     * @param ignoreOutsideMinMax if true, the data outside [min,max) interval are ignored (provided max &gt; min) if false, the data outside [min,max) "goes
     *                            to" the nearest bin (provided max &gt; min)
     * @param mask                mask (can be null)
     *
     * @return the histogram of the current time step.
     */
    public long[] getCurrentHistogram(final double min, final double max, final int nbin, boolean ignoreOutsideMinMax, TimeData mask)
    {
        return histogram(this, min, max, nbin, ignoreOutsideMinMax, mask, false);
    }

    /**
     * Returns the histogram of all time steps.
     *
     * @return the histogram of all time steps.
     */
    public long[] getHistogram()
    {
        return getHistogram(getPreferredMinValue(), getPreferredMaxValue(), 256, false, null);
    }

    /**
     * Returns the histogram of all time steps. The data outside [min,max) interval are ignored.
     *
     * @param min                 min value
     * @param max                 max value
     * @param nbin                number of bins
     * @param ignoreOutsideMinMax if true, the data outside [min,max) interval are ignored (provided max &gt; min) if false, the data outside [min,max) "goes
     *                            to" the nearest bin (provided max &gt; min)
     * @param mask                mask (can be null)
     *
     * @return the histogram of all time steps.
     */
    public long[] getHistogram(final double min, final double max, final int nbin, boolean ignoreOutsideMinMax, TimeData mask)
    {
        return histogram(this, min, max, nbin, ignoreOutsideMinMax, mask, true);
    }

    /**
     * Returns a 1D slice.
     *
     * @param start number of elements to skip from the beginning of the input array
     * @param n     number of elements in the output slice
     * @param step  distance between consecutive elements in the input array
     *
     * @return 1D slice
     */
    public DataArray get1DSlice(final long start, final long n, final long step)
    {
        TimeData slicedTimeData = timeData.get1DSlice(start, n, step, getVectorLength());
        DataArray da = create(slicedTimeData.getType(), slicedTimeData.length() / getVectorLength(), getVectorLength(), getName(), getUnit(), getUserData());
        da.setTimeData(slicedTimeData);
        da.setCurrentTime(getCurrentTime());
        return da;
    }

    /**
     * Returns a 1D slice of the current time step.
     *
     * @param start number of elements to skip from the beginning of the input array
     * @param n     number of elements in the output slice
     * @param step  distance between consecutive elements in the input array
     *
     * @return 1D slice of the current time step
     */
    public LargeArray getCurrent1DSlice(final long start, final long n, final long step)
    {
        return timeData.getCurrent1DSlice(start, n, step, getVectorLength());
    }

    /**
     * Returns a 1D float slice.
     *
     * @param start number of elements to skip from the beginning of the input array
     * @param n     number of elements in the output slice
     * @param step  distance between consecutive elements in the input array
     *
     * @return 1D float slice
     */
    public DataArray get1DFloatSlice(final long start, final long n, final long step)
    {
        TimeData slicedTimeData = timeData.get1DFloatSlice(start, n, step, getVectorLength());
        DataArray da = create(slicedTimeData.getType(), slicedTimeData.length() / getVectorLength(), getVectorLength(), getName(), getUnit(), getUserData());
        da.setTimeData(slicedTimeData);
        da.setCurrentTime(getCurrentTime());
        return da;
    }

    /**
     * Returns a 1D float slice of the current time step.
     *
     * @param start number of elements to skip from the beginning of the input array
     * @param n     number of elements in the output slice
     * @param step  distance between consecutive elements in the input array
     *
     * @return 1D slice float of the current time step
     */
    public FloatLargeArray getCurrent1DFloatSlice(final long start, final long n, final long step)
    {
        return timeData.getCurrent1DFloatSlice(start, n, step, getVectorLength());
    }

    /**
     * Returns a 1D slice of norms.
     *
     * @param start number of elements to skip from the beginning of the input array
     * @param n     number of elements in the output slice
     * @param step  distance between consecutive elements in the input array
     *
     * @return 1D slice of norms
     */
    public DataArray get1DNormSlice(final long start, final long n, final long step)
    {
        TimeData slicedTimeData = timeData.get1DNormSlice(start, n, step, getVectorLength());
        DataArray da = create(slicedTimeData.getType(), slicedTimeData.length() / getVectorLength(), getVectorLength(), getName(), getUnit(), getUserData());
        da.setTimeData(slicedTimeData);
        da.setCurrentTime(getCurrentTime());
        return da;
    }

    /**
     * Returns a 1D slice of norms of the current time step.
     *
     * @param start number of elements to skip from the beginning of the input array
     * @param n     number of elements in the output slice
     * @param step  distance between consecutive elements in the input array
     *
     * @return 1D slice float of the current time step
     */
    public FloatLargeArray getCurrent1DNormSlice(final long start, final long n, final long step)
    {
        return timeData.getCurrent1DNormSlice(start, n, step, getVectorLength());
    }

    /**
     * Returns a float element at specified position.
     *
     * @param n position
     *
     * @return a float element
     */
    public float[] getFloatElement(long n)
    {
        int veclen = schema.getVectorLength();
        float[] out = new float[veclen];
        long i, j;
        for (i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = timeData.getCurrentValue().getFloat(j);
        }
        return out;
    }

    /**
     * Returns a physical float element at specified position.
     *
     * @param n position
     *
     * @return a float element
     */
    public float[] getPhysicalFloatElement(long n)
    {
        int veclen = schema.getVectorLength();
        float[] out = new float[veclen];
        long i, j;
        for (i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = (float) schema.dataRawToPhys(timeData.getCurrentValue().getFloat(j));
        }
        return out;
    }

    /**
     * Returns a double element at specified position.
     *
     * @param n position
     *
     * @return a float element
     */
    public double[] getDoubleElement(long n)
    {
        int veclen = schema.getVectorLength();
        double[] out = new double[veclen];
        long i, j;
        for (i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = timeData.getCurrentValue().getDouble(j);
        }
        return out;
    }

    /**
     * Returns a physical double element at specified position.
     *
     * @param n position
     *
     * @return a float element
     */
    public double[] getPhysicalDoubleElement(long n)
    {
        int veclen = schema.getVectorLength();
        double[] out = new double[veclen];
        long i, j;
        for (i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = schema.dataRawToPhys(timeData.getCurrentValue().getDouble(j));
        }
        return out;
    }

    /**
     * Returns a subcomponent for the given vector index v.
     *
     * @param v vector index
     *
     * @return a subcomponent for the given vector index v.
     */
    public DataArray getSubcomponent(int v)
    {
        int veclen = getVectorLength();
        if (v < 0 || v > veclen - 1) {
            throw new IllegalArgumentException("v < 0 || v > veclen - 1");
        }
        if (veclen == 1) {
            return this;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) timeData.getTimesAsList();
        int size = timeSeries.size();
        long len = getNElements();
        ArrayList<LargeArray> timeValues = new ArrayList<>(size);
        int k = 0;
        for (Float time : timeSeries) {
            LargeArray temp = timeData.getValue(time);
            LargeArray la = LargeArrayUtils.create(getType().toLargeArrayType(), len, false);
            for (long j = 0; j < len; j++) {
                la.set(j, temp.get(j * veclen + v));
            }
            timeValues.add(k++, la);
        }
        return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), 1, getName() + "." + Integer.toString(v), getUnit(), getUserData());
    }

    /**
     * Returns an array of the Euclidean norm of each element.
     *
     * @return array of the Euclidean norm of each element
     */
    public FloatLargeArray getVectorNorms()
    {
        final int veclen = schema.getVectorLength();
        return LargeArrayMath.vectorNorms(timeData.getCurrentValue(), veclen);
    }

    /**
     * Returns a raw byte array. This method always creates a copy of the data.
     *
     * @return raw byte array
     */
    public UnsignedByteLargeArray getRawByteArray()
    {
        if (getType() == DataArrayType.FIELD_DATA_BYTE) {
            return (UnsignedByteLargeArray) timeData.getCurrentValue().clone();
        }
        return convertToUnsignedByteLargeArray(timeData.getCurrentValue(), false, 0, 1);
    }

    /**
     * Returns a raw short array. This method always creates a copy of the data.
     *
     * @return raw short array
     */
    public ShortLargeArray getRawShortArray()
    {
        if (getType() == DataArrayType.FIELD_DATA_SHORT) {
            return (ShortLargeArray) timeData.getCurrentValue().clone();
        }
        return convertToShortLargeArray(timeData.getCurrentValue(), false, 0, 1);
    }

    /**
     * Returns a raw int array. This method always creates a copy of the data.
     *
     * @return raw int array
     */
    public IntLargeArray getRawIntArray()
    {
        if (getType() == DataArrayType.FIELD_DATA_INT) {
            return (IntLargeArray) timeData.getCurrentValue().clone();
        }
        return convertToIntLargeArray(timeData.getCurrentValue(), false, 0, 1);
    }

    /**
     * Returns a raw long array. This method always creates a copy of the data.
     *
     * @return raw long array
     */
    public LongLargeArray getRawLongArray()
    {
        if (getType() == DataArrayType.FIELD_DATA_LONG) {
            return (LongLargeArray) timeData.getCurrentValue().clone();
        }
        return convertToLongLargeArray(timeData.getCurrentValue(), false, 0, 1);
    }

    /**
     * Returns a raw float array. This method always creates a copy of the data.
     *
     * @return raw float array
     */
    public FloatLargeArray getRawFloatArray()
    {
        if (getType() == DataArrayType.FIELD_DATA_FLOAT) {
            return (FloatLargeArray) timeData.getCurrentValue().clone();
        }
        return convertToFloatLargeArray(timeData.getCurrentValue());
    }

    /**
     * Returns a physical float array. This method always creates a copy of the data.
     *
     * @return physical float array
     */
    public FloatLargeArray getPhysicalFloatArray()
    {
        if (schema.getPreferredMinValue() == schema.getPreferredPhysMinValue() && schema.getPreferredMaxValue() == schema.getPreferredPhysMaxValue()) {
            return getRawFloatArray();
        }

        final FloatLargeArray res = getRawFloatArray();
        long n = res.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        res.setFloat(i, (float) schema.dataRawToPhys(res.getFloat(i)));
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            for (long i = 0; i < n; i++) {
                res.setFloat(i, (float) schema.dataRawToPhys(res.getFloat(i)));
            }
        }
        return res;

    }

    /**
     * Returns a raw double array. This method always creates a copy of the data.
     *
     * @return raw double array
     */
    public DoubleLargeArray getRawDoubleArray()
    {
        if (getType() == DataArrayType.FIELD_DATA_DOUBLE) {
            return (DoubleLargeArray) timeData.getCurrentValue().clone();
        }
        return convertToDoubleLargeArray(timeData.getCurrentValue());
    }

    /**
     * Returns a physical double array. This method always creates a copy of the data.
     *
     * @return physical double array
     */
    public DoubleLargeArray getPhysicalDoubleArray()
    {
        if (schema.getPreferredMinValue() == schema.getPreferredPhysMinValue() && schema.getPreferredMaxValue() == schema.getPreferredPhysMaxValue()) {
            return getRawDoubleArray();
        }

        final DoubleLargeArray res = getRawDoubleArray();
        long n = res.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        res.setDouble(i, schema.dataRawToPhys(res.getDouble(i)));
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            for (long i = 0; i < n; i++) {
                res.setDouble(i, schema.dataRawToPhys(res.getDouble(i)));
            }
        }
        return res;
    }

    /**
     * Returns a raw array. This method always returns a reference to the internal DataArray.
     *
     * @return reference to a raw array
     */
    abstract public LargeArray getRawArray();

    /**
     * Returns the DataArray at specified time step. If the specified time step does not exist, then an empty DataArray is created and returned.
     *
     * @param time time step
     *
     * @return DataArray at specified time step
     */
    public LargeArray produceData(float time)
    {
        return (LargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

    /**
     * Returns the DataArray at specified time step. If the specified time step does not exist, then an interpolated (via linear interpolation) DataArray is
     * returned.
     *
     * @param time time step
     *
     * @return DataArray at specified time step
     */
    public LargeArray getRawArray(float time)
    {
        return (LargeArray) timeData.getValue(time);
    }

    /**
     * Returns the value of user data.
     *
     * @return the value of user data
     */
    public String[] getUserData()
    {
        return schema.getUserData();
    }

    /**
     * Get the value of user data at specified index.
     *
     * @param index index
     *
     * @return the value of userData at specified index
     */
    public String getUserData(int index)
    {
        return schema.getUserData(index);
    }

    /**
     * Set the value of user data at specified index.
     *
     * @param index       index
     * @param newUserData new value of user data at specified index
     *
     * @return itself
     */
    public DataArray setUserData(int index, String newUserData)
    {
        schema.setUserData(index, newUserData);
        this.timestamp = System.nanoTime();
        return this;
    }

    /**
     * Sets the value of user data.
     *
     * @param userData new value of user data
     */
    public void setUserData(String[] userData)
    {
        schema.setUserData(userData);
        this.timestamp = System.nanoTime();
    }

    /**
     * Sets the value of user data.
     *
     * @param userData new value of user data
     *
     * @return itself
     */
    public DataArray userData(String[] userData)
    {
        setUserData(userData);
        return this;
    }

    /**
     * Returns the value of a hint for data mapping methods. If it is set to true, then low and up bounds for data mapping are automatically set to physical
     * minimum and physical maximum when array is refreshed.
     *
     * @return value of a hint for data mapping methods
     */
    public boolean isAutoResetMapRange()
    {
        return schema.isAutoResetMapRange();
    }

    /**
     * Sets the value of a hint for data mapping methods. If it is set to true, then low and up bounds for data mapping are automatically set to physical
     * minimum and physical maximum when array is refreshed.
     *
     * @param autoResetMapRange of a hint for data mapping methods
     */
    public void setAutoResetMapRange(boolean autoResetMapRange)
    {
        schema.setAutoResetMapRange(autoResetMapRange);
    }

    /**
     * Sets the value of a hint for data mapping methods. If it is set to true, then low and up bounds for data mapping are automatically set to physical
     * minimum and physical maximum when array is refreshed.
     *
     * @param autoResetMapRange of a hint for data mapping methods
     *
     * @return itself
     */
    public DataArray autoResetMapRange(boolean autoResetMapRange)
    {
        setAutoResetMapRange(autoResetMapRange);
        return this;
    }

    /**
     * Sets the System.nanoTime() as the value of the timestamp
     */
    public void updateTimestamp()
    {
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if the DataArray changed since the give timestamp, false otherwise.
     *
     * @param timestamp timestamp
     *
     * @return true if the DataArray changed since the given timestamp, false otherwise
     */
    public boolean changedSince(long timestamp)
    {
        return this.timestamp > timestamp;
    }

    /**
     * Returns the current timestamp.
     *
     * @return timestamp
     */
    public long getTimestamp()
    {
        return timestamp;
    }

    /**
     * Sets the new value of time data. The argument value is not copied.
     *
     * @param tData new value of time data
     */
    public void setTimeData(TimeData tData)
    {
        if (tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()) {
            throw new IllegalArgumentException("tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()");
        }

        timeData = tData;
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    /**
     * Retuns a timestep corresponding to a given frame.
     *
     * @param frame frame
     *
     * @return timestep
     */
    public float getTime(int frame)
    {
        return timeData.getTime(frame);
    }

    /**
     * Returns a current timestep.
     *
     * @return current timestep
     */
    public float getCurrentTime()
    {
        return timeData.getCurrentTime();
    }

    /**
     * Sets current timestep.
     *
     * @param currentTime current timestep.
     */
    public void setCurrentTime(float currentTime)
    {
        if (!timeData.isEmpty() && currentTime == timeData.getCurrentTime()) {
            return;

        }
        timeData.setCurrentTime(currentTime);
        timestamp = System.nanoTime();
    }

    /**
     * Returns the first timestep.
     *
     * @return first timestep
     */
    public float getStartTime()
    {
        return timeData.getStartTime();
    }

    /**
     * Returns the last timestep.
     *
     * @return last timestep.
     */
    public float getEndTime()
    {
        return timeData.getEndTime();
    }

    /**
     * Checks if the specified timestep exists.
     *
     * @param t timestep
     *
     * @return true, if the specified timestep exists, false otherwise.
     */
    public boolean isTimestep(float t)
    {
        return timeData != null && timeData.isTimestep(t);
    }

    /**
     * Returns true if the DataArray is time dependent, false otherwise.
     *
     * @return true if the DataArray is time dependent, false otherwise
     */
    public boolean isTimeDependant()
    {
        return (timeData.getNSteps() > 1);
    }

    /**
     * Returns time series.
     *
     * @return time series.
     */
    public ArrayList<Float> getTimeSeries()
    {
        return timeData.getTimesAsList();
    }

    /**
     * Returns a 2D slice.
     *
     * @param dims  dimensions of the field.
     * @param axis  axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return a 2D slice.
     */
    public DataArray get2DSlice(long[] dims, int axis, long slice)
    {
        if (dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]) {
            throw new IllegalArgumentException("dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]");
        }
        TimeData slicedTimeData = timeData.get2DSlice(dims, axis, slice, getVectorLength());
        DataArray da = create(getType(), getNElements() / dims[axis], getVectorLength(), getName(), getUnit(), getUserData());
        da.setTimeData(slicedTimeData);
        da.setCurrentTime(getCurrentTime());
        return da;
    }

    /**
     * Returns 2D slice of the current time step.
     *
     * @param dims  dimensions of the field.
     * @param axis  axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return 2D slice of the current time step
     */
    public LargeArray getCurrent2DSlice(long[] dims, int axis, long slice)
    {
        if (dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]) {
            throw new IllegalArgumentException("dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]");
        }
        return timeData.getCurrent2DSlice(dims, axis, slice, getVectorLength());
    }

    /**
     * Returns a 2D float slice.
     *
     * @param dims  dimensions of the field.
     * @param axis  axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return a 2D float slice.
     */
    public DataArray get2DFloatSlice(long[] dims, int axis, long slice)
    {
        if (dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]) {
            throw new IllegalArgumentException("dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]");
        }
        TimeData slicedTimeData = timeData.get2DFloatSlice(dims, axis, slice, getVectorLength());
        DataArray da = create(slicedTimeData.getType(), getNElements() / dims[axis], getVectorLength(), getName(), getUnit(), getUserData());
        da.setTimeData(slicedTimeData);
        da.setCurrentTime(getCurrentTime());
        return da;
    }

    /**
     * Returns 2D float slice of the current time step.
     *
     * @param dims  dimensions of the field.
     * @param axis  axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return 2D float slice of the current time step
     */
    public FloatLargeArray getCurrent2DFloatSlice(long[] dims, int axis, long slice)
    {
        if (dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]) {
            throw new IllegalArgumentException("dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]");
        }
        return timeData.getCurrent2DFloatSlice(dims, axis, slice, getVectorLength());
    }

    /**
     * Returns a 2D slice of norms.
     *
     * @param dims  dimensions of the field.
     * @param axis  axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return a 2D slice of norms.
     */
    public DataArray get2DNormSlice(long[] dims, int axis, long slice)
    {
        if (dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]) {
            throw new IllegalArgumentException("dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]");
        }
        TimeData slicedTimeData = timeData.get2DNormSlice(dims, axis, slice, getVectorLength());
        DataArray da = create(slicedTimeData.getType(), getNElements() / dims[axis], 1, getName(), getUnit(), getUserData());
        da.setTimeData(slicedTimeData);
        da.setCurrentTime(getCurrentTime());
        return da;
    }

    /**
     * Returns 2D slice of norms of the current time step.
     *
     * @param dims  dimensions of the field.
     * @param axis  axis specification: 0, 1, or 2
     * @param slice slice number
     *
     * @return 2D float slice of the current time step
     */
    public FloatLargeArray getCurrent2DNormSlice(long[] dims, int axis, long slice)
    {
        if (dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]) {
            throw new IllegalArgumentException("dims == null || dims.length != 3 || axis < 0 || axis > 2 || slice < 0 || slice >= dims[axis]");
        }
        return timeData.getCurrent2DNormSlice(dims, axis, slice, getVectorLength());
    }

    /**
     * Basic comparator for DataArray compatibility
     *
     * @param s                   - DataArray to be checked for compatibility
     *
     * @param checkComponentNames if true, then the names of the components are compared
     *
     * @return - true if name, type, veclen, units and time vectors of s are compatible
     */
    public boolean compatibleWith(DataArray s, boolean checkComponentNames)
    {
        if (!schema.isCompatibleWith(s.getSchema(), checkComponentNames)) {
            return false;
        }
        ArrayList<Float> tSeries = getTimeSeries();
        ArrayList<Float> sSeries = s.getTimeSeries();
        if (sSeries.size() != tSeries.size()) {
            return false;
        }
        for (int i = 0; i < tSeries.size(); i++) {
            if (!Objects.equals(tSeries.get(i), sSeries.get(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if the number of elements in the array is bigger than LargeArray.getMaxSizeOf32bitArray(), false otherwise.
     *
     * @return true if the number of elements in the array is bigger than LargeArray.getMaxSizeOf32bitArray(), false otherwise
     */
    public boolean isLarge()
    {
        return timeData.getCurrentValue().isLarge();
    }

    /**
     * Returns true if the DataArray has a parent, false otherwise.
     *
     * @return true if the DataArray has a parent, false otherwise
     */
    public boolean hasParent()
    {
        return hasParent;
    }

    /**
     * Sets the flag hasParent.
     *
     * @param hasParent parent flag.
     */
    public void setHasParent(boolean hasParent)
    {
        this.hasParent = hasParent;
        timestamp = System.nanoTime();
    }

    /**
     * Returns time data.
     *
     * @return time data.
     */
    public TimeData getTimeData()
    {
        return timeData;
    }
}
