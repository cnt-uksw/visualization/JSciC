/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import ucar.units.ConversionException;
import ucar.units.PrefixDBException;
import ucar.units.SpecificationException;
import ucar.units.Unit;
import ucar.units.UnitDBException;
import ucar.units.UnitFormat;
import ucar.units.UnitFormatManager;
import ucar.units.UnitSystemException;

/**
 * Operations on units.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class UnitUtils
{

    /**
     * Parses a unit specification and returns the unit corresponding to the specification.
     *
     * @param unit unit specification
     *
     * @return the unit corresponding to the specification
     */
    public static Unit getUnit(String unit)
    {
        if (!isValidUnit(unit)) {
            throw new IllegalArgumentException("Invalid unit " + unit);
        }
        UnitFormat format = UnitFormatManager.instance();
        try {
            return format.parse(unit);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Invalid unit " + unit);
        }
    }

    /**
     * Returns true if the unit specification represents a valid unit, false otherwise.
     *
     * @param unit unit specification
     *
     * @return true if the unit specification represents a valid unit, false otherwise
     */
    public static boolean isValidUnit(String unit)
    {
        if (unit == null || unit.isEmpty() || unit.equals("1")) {
            return false;
        }
        UnitFormat format = UnitFormatManager.instance();
        try {
            format.parse(unit);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            return false;
        }
        return true;
    }

    /**
     * Returns true if both unit specifications represent valid and compatible units, false otherwise.
     *
     * @param unit1 unit specification
     * @param unit2 unit specification
     *
     * @return true if both unit specifications represent valid and compatible units, false otherwise
     */
    public static boolean areValidAndCompatibleUnits(String unit1, String unit2)
    {
        if (!isValidUnit(unit1) || !isValidUnit(unit2)) {
            return false;
        }
        UnitFormat format = UnitFormatManager.instance();
        Unit u1, u2;
        try {
            u1 = format.parse(unit1);
            u2 = format.parse(unit2);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            return false;
        }
        return u1.isCompatible(u2);
    }

    /**
     * Returns true if both unit specifications represent the same and valid unit, false otherwise.
     *
     * @param unit1 unit specification
     * @param unit2 unit specification
     *
     * @return true if both unit specifications represent the same and valid unit, false otherwise
     */
    public static boolean areSameUnits(String unit1, String unit2)
    {
        if (!isValidUnit(unit1) || !isValidUnit(unit2)) {
            return false;
        }
        UnitFormat format = UnitFormatManager.instance();
        Unit u1, u2;
        try {
            u1 = format.parse(unit1);
            u2 = format.parse(unit2);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            return false;
        }
        return u1.equals(u2);
    }

    /**
     * Returns the derived unit that underlies the unit specification.
     *
     * @param unit unit specification
     *
     * @return the derived unit that underlies the unit specification
     */
    public static String getDerivedUnit(String unit)
    {
        if (!isValidUnit(unit)) {
            throw new IllegalArgumentException("Invalid unit");
        }
        UnitFormat format = UnitFormatManager.instance();
        Unit u;

        try {
            u = format.parse(unit);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Invalid unit");
        }

        return u.getDerivedUnit().toString();
    }

    /**
     * Converts prefferedPhysicalMin and prefferedPhysicalMax values of the data array to a given unit specification.
     *
     * @param da         data array
     * @param outUnitStr unit specification
     *
     * @return shallow copy of the input data array with updated prefferedPhysicalMin and prefferedPhysicalMax.
     */
    public static DataArray shallowUnitConvert(DataArray da, String outUnitStr)
    {
        return unitConvertF(da, outUnitStr, false);
    }

    /**
     * Converts prefferedPhysicalMin, prefferedPhysicalMax and data values of the data array to a given unit specification.
     *
     * @param da         data array
     * @param outUnitStr unit specification
     *
     * @return a new data array of type float if conversion was necessary, input array otherwise
     */
    public static DataArray deepUnitConvertF(DataArray da, String outUnitStr)
    {
        return unitConvertF(da, outUnitStr, true);
    }

    /**
     * Converts prefferedPhysicalMin, prefferedPhysicalMax and data values of the data array to a given unit specification.
     *
     * @param da         data array
     * @param outUnitStr unit specification
     *
     * @return a new data array of type double if conversion was necessary, input array otherwise
     */
    public static DataArray deepUnitConvertD(DataArray da, String outUnitStr)
    {
        return unitConvertD(da, outUnitStr, true);
    }

    /**
     * Converts a scalar value using input and output unit specifications.
     *
     * @param value      input value
     * @param inUnitStr  input unit specification
     * @param outUnitStr output unit specification
     *
     * @return value in converted to the output unit
     */
    public static float unitConvert(float value, String inUnitStr, String outUnitStr)
    {
        if (!isValidUnit(inUnitStr) || !isValidUnit(outUnitStr)) {
            throw new IllegalArgumentException("Input or output units are invalid.");
        }
        UnitFormat format = UnitFormatManager.instance();
        Unit u1, u2;
        try {
            u1 = format.parse(inUnitStr);
            u2 = format.parse(outUnitStr);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Input or output units are invalid.");
        }
        if (!u1.isCompatible(u2)) {
            throw new IllegalArgumentException("Input and output units are not compatible.");
        }
        try {
            return u1.convertTo(value, u2);
        } catch (ConversionException ex) {
            throw new IllegalArgumentException("Input or output units are invalid.");
        }
    }

    /**
     * Converts a scalar value using input and output unit specifications.
     *
     * @param value      input value
     * @param inUnitStr  input unit specification
     * @param outUnitStr output unit specification
     *
     * @return value in converted to the output unit
     */
    public static double unitConvert(double value, String inUnitStr, String outUnitStr)
    {
        if (!isValidUnit(inUnitStr) || !isValidUnit(outUnitStr)) {
            throw new IllegalArgumentException("Input or output units are invalid.");
        }
        UnitFormat format = UnitFormatManager.instance();
        Unit u1, u2;
        try {
            u1 = format.parse(inUnitStr);
            u2 = format.parse(outUnitStr);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Input or output units are invalid.");
        }
        if (!u1.isCompatible(u2)) {
            throw new IllegalArgumentException("Input and output units are not compatible.");
        }
        try {
            return u1.convertTo(value, u2);
        } catch (ConversionException ex) {
            throw new IllegalArgumentException("Input or output units are invalid.");
        }
    }

    private static DataArray unitConvertF(DataArray da, String outUnitStr, boolean deepConversion)
    {
        if (da.isUnitless()) {
            throw new IllegalArgumentException("Input array is unitless.");
        }

        if (!da.isNumeric()) {
            throw new IllegalArgumentException("Input array is not numeric.");
        }

        if (!isValidUnit(outUnitStr)) {
            throw new IllegalArgumentException("Invalid output unit " + outUnitStr);
        }

        String inUnitStr = da.getUnit();
        UnitFormat format = UnitFormatManager.instance();
        Unit inUnit, outUnit;
        double[] unitCoeffs;
        double[] physCoeffs;
        double oldPreferredPhysicalMin, oldPreferredPhysicalMax;
        double newPreferredPhysicalMin = 0, newPreferredPhysicalMax = 0;

        DataArray out;

        try {
            inUnit = format.parse(inUnitStr);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Invalid input unit " + inUnitStr);
        }
        try {
            outUnit = format.parse(outUnitStr);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Invalid output unit " + outUnitStr);
        }

        if (!inUnit.isCompatible(outUnit)) {
            throw new IllegalArgumentException("Input and output units are not compatible.");
        }

        if (inUnit.equals(outUnit)) return da;

        oldPreferredPhysicalMin = da.getPreferredPhysMinValue();
        oldPreferredPhysicalMax = da.getPreferredPhysMaxValue();

        try {
            newPreferredPhysicalMin = inUnit.convertTo(oldPreferredPhysicalMin, outUnit);
            newPreferredPhysicalMax = inUnit.convertTo(oldPreferredPhysicalMax, outUnit);
        } catch (ConversionException ex) {
            throw new IllegalStateException("Exception occured during unit conversion.");
        }

        if (!deepConversion) {
            out = da.cloneShallow();
            out.setPreferredRanges(da.getPreferredMinValue(), da.getPreferredMaxValue(), newPreferredPhysicalMin, newPreferredPhysicalMax);
            out.setUnit(outUnitStr);
        } else {
            unitCoeffs = ScalarMath.linearMappingCoefficients(oldPreferredPhysicalMin, oldPreferredPhysicalMax, newPreferredPhysicalMin, newPreferredPhysicalMax);
            physCoeffs = ScalarMath.linearMappingCoefficients(0, 1, da.getSchema().dataRawToPhys(0), da.getSchema().dataRawToPhys(1));
            if (unitCoeffs[0] * physCoeffs[0] == 1.0 && unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1] == 0) {
                return da;
            } else if (unitCoeffs[0] * physCoeffs[0] == 1.0) {
                out = DataArrayArithmetics.addF(da, DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, da.getNElements(), unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1]), true);
            } else if (unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1] == 0) {
                out = DataArrayArithmetics.multF(DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, da.getNElements(), unitCoeffs[0] * physCoeffs[0]), da, true);
            } else {
                out = DataArrayArithmetics.axpyF(DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, da.getNElements(), unitCoeffs[0] * physCoeffs[0]), da, DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, da.getNElements(), unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1]), true);
            }
            out.setUnit(outUnitStr);
        }

        return out;
    }

    private static DataArray unitConvertD(DataArray da, String outUnitStr, boolean deepConversion)
    {
        if (da.isUnitless()) {
            throw new IllegalArgumentException("Input array is unitless.");
        }

        if (!da.isNumeric()) {
            throw new IllegalArgumentException("Input array is not numeric.");
        }

        if (!isValidUnit(outUnitStr)) {
            throw new IllegalArgumentException("Invalid output unit " + outUnitStr);
        }

        String inUnitStr = da.getUnit();
        UnitFormat format = UnitFormatManager.instance();
        Unit inUnit, outUnit;
        double[] unitCoeffs;
        double[] physCoeffs;
        double oldPreferredPhysicalMin, oldPreferredPhysicalMax;
        double newPreferredPhysicalMin = 0, newPreferredPhysicalMax = 0;

        DataArray out = null;

        try {
            inUnit = format.parse(inUnitStr);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Invalid input unit " + inUnitStr);
        }
        try {
            outUnit = format.parse(outUnitStr);
        } catch (SpecificationException | UnitDBException | PrefixDBException | UnitSystemException ex) {
            throw new IllegalArgumentException("Invalid output unit " + outUnitStr);
        }

        if (!inUnit.isCompatible(outUnit)) {
            throw new IllegalArgumentException("Input and output units are not compatible.");
        }

        if (inUnit.equals(outUnit)) return da;

        oldPreferredPhysicalMin = da.getPreferredPhysMinValue();
        oldPreferredPhysicalMax = da.getPreferredPhysMaxValue();

        try {
            newPreferredPhysicalMin = inUnit.convertTo(oldPreferredPhysicalMin, outUnit);
            newPreferredPhysicalMax = inUnit.convertTo(oldPreferredPhysicalMax, outUnit);
        } catch (ConversionException ex) {
            throw new IllegalStateException("Exception occured during unit conversion.");
        }

        if (!deepConversion) {
            out = da.cloneShallow();
            out.setPreferredRanges(da.getPreferredMinValue(), da.getPreferredMaxValue(), newPreferredPhysicalMin, newPreferredPhysicalMax);
            out.setUnit(outUnitStr);
        } else {
            unitCoeffs = ScalarMath.linearMappingCoefficients(oldPreferredPhysicalMin, oldPreferredPhysicalMax, newPreferredPhysicalMin, newPreferredPhysicalMax);
            physCoeffs = ScalarMath.linearMappingCoefficients(0, 1, da.getSchema().dataRawToPhys(0), da.getSchema().dataRawToPhys(1));
            if (unitCoeffs[0] * physCoeffs[0] == 1.0 && unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1] == 0) {
                return da;
            } else if (unitCoeffs[0] * physCoeffs[0] == 1.0) {
                out = DataArrayArithmetics.addD(da, DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, da.getNElements(), unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1]), true);
            } else if (unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1] == 0) {
                out = DataArrayArithmetics.multD(DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, da.getNElements(), unitCoeffs[0] * physCoeffs[0]), da, true);
            } else {
                out = DataArrayArithmetics.axpyD(DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, da.getNElements(), unitCoeffs[0] * physCoeffs[0]), da, DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, da.getNElements(), unitCoeffs[0] * physCoeffs[1] + unitCoeffs[1]), true);
            }
            out.setUnit(outUnitStr);
        }

        return out;
    }
}
