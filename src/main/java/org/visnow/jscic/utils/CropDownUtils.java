/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 * Array cropping and downsizing utilities.
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class CropDownUtils
{

    /**
     * Crops and downsizes a LargeArray.
     *
     * @param data   input array
     * @param veclen vector length
     * @param dims   dimensions of the input array
     * @param low    amount of cropping from the beginning in each dimension
     * @param up     amount of cropping from the end in each dimension
     * @param down   amount of downsizing in each dimension
     *
     * @return cropped and downsized array
     */
    public static LargeArray cropDownArray(LargeArray data, int veclen, int[] dims, int[] low, int[] up, int[] down)
    {

        long n = 1;
        for (int i = 0; i < dims.length; i++) {
            if (low[i] < 0 || up[i] < 0 || (up[i] - low[i] - 1 <= 0) || veclen <= 0 || down[i] <= 0) {
                throw new IllegalArgumentException("low[i] < 0 || up[i] < 0 || (up[i] - low[i] - 1 <= 0) || veclen <= 0 || down[i] <= 0");
            }
            n *= ((long) up[i] - (long) low[i] - 1) / (long) down[i] + 1;
        }

        LargeArray dt;
        if (data.getType() == LargeArrayType.STRING) {
            dt = new StringLargeArray(n * veclen, DataArray.MAX_STRING_LENGTH, false);
        } else if (data.getType() == LargeArrayType.OBJECT) {
            dt = new ObjectLargeArray(n * veclen, DataArray.MAX_OBJECT_SIZE, false);
        } else {
            dt = LargeArrayUtils.create(data.getType(), n * veclen, false);
        }

        switch (dims.length) {
            case 3:
                for (long i = low[2], l = 0; i < up[2]; i += down[2])
                    for (long j = low[1]; j < up[1]; j += down[1])
                        for (long k = low[0], m = (i * dims[1] + j) * dims[0]; k < up[0]; k += down[0], l += veclen)
                            for (long c = 0; c < veclen; c++)
                                dt.set(l + c, data.get((m + k) * veclen + c));
                break;
            case 2:
                for (long j = low[1], l = 0; j < up[1]; j += down[1])
                    for (long k = low[0], m = j * dims[0]; k < up[0]; k += down[0], l += veclen)
                        for (long c = 0; c < veclen; c++)
                            dt.set(l + c, data.get((m + k) * veclen + c));
                break;
            case 1:
                for (long k = low[0], l = 0; k < up[0]; k += down[0], l += veclen)
                    for (long c = 0; c < veclen; c++)
                        dt.set(l + c, data.get(k * veclen + c));
                break;
        }
        return dt;
    }

    /**
     * Downsizes a LargeArray.
     *
     * @param data   input array
     * @param veclen vector length
     * @param dims   dimensions of the input array
     * @param down   amount of downsizing in each dimension
     *
     * @return downsized array
     */
    public static LargeArray downArray(LargeArray data, int veclen, int[] dims, int[] down)
    {
        return cropDownArray(data, veclen, dims, new int[]{0, 0, 0}, dims, down);
    }

    private CropDownUtils()
    {

    }

}
