/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import static org.apache.commons.math3.util.FastMath.max;
import static org.apache.commons.math3.util.FastMath.min;
import org.visnow.jlargearrays.LargeArray;
import static org.visnow.jlargearrays.LargeArrayUtils.*;

/**
 * Array tile utilities.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TileUtils
{

    /**
     * Copies the tile data into the target array.
     *
     * @param tile    tile specification
     *                (low, up, step)i , (low, up, step)j, (low, up, step)k, where low and up are
     *                given in original resolution, up-low+1 is the tile dimension, step is downsize step
     * @param dims    dimensions of the target array
     * @param target  target array
     * @param content content of a tile
     * @param veclen  vector length of the target array
     */
    public static void putTile(int[][] tile, int[] dims, LargeArray target, LargeArray content, int veclen)
    {
        if (target == null || content == null || target.getType() != content.getType()) {
            throw new IllegalArgumentException("target == null || content == null || target.getType() != content.getType()");
        }
        long tileSize = 1;
        int iDown = 1, jDown = 1, kDown = 1;
        int[] tileDims = new int[dims.length];
        for (int i = 0; i < dims.length; i++) {
            if (tile[i].length < 2 || tile[i][0] > tile[i][1])
                throw new IllegalArgumentException("tile[i].length < 2 || tile[i][0] > tile[i][1]");
            tileDims[i] = tile[i][1] - tile[i][0] + 1;
            tileSize *= tileDims[i];
        }
        if (tile[0].length > 2)
            kDown = tile[0][2];
        if (tile.length > 1 && tile[1].length > 2)
            jDown = tile[1][2];
        if (tile.length > 2 && tile[2].length > 2)
            iDown = tile[2][2];
        if (content.length() != veclen * tileSize)
            throw new IllegalArgumentException("content.length != veclen * k");
        long nCopied = veclen * (min(tile[0][1] + 1, dims[0]) - max(tile[0][0], 0));
        long[] dimsl = new long[dims.length];
        for (int i = 0; i < dimsl.length; i++)
            dimsl[i] = dims[i];
        switch (dims.length) {
            case 3:
                for (long ii = tile[2][0] >= 0 ? tile[2][0] % iDown : -tile[2][0],
                    i = max((tile[2][0] + iDown - 1) / iDown, 0);
                    ii < tileDims[2] && i < dimsl[2];
                    ii += iDown, i++)
                    for (long jj = tile[1][0] >= 0 ? tile[1][0] % jDown : -tile[1][0],
                        j = max((tile[1][0] + jDown - 1) / jDown, 0);
                        jj < tileDims[1] && j < dimsl[1];
                        jj += jDown, j++)
                        if (kDown == 1)
                            arraycopy(content, (int) (veclen * ((tileDims[1] * ii + jj) * tileDims[0] + max(-tile[0][0], 0))),
                                      target, veclen * ((dimsl[1] * i + j) * dimsl[0] + max(tile[0][0], 0)),
                                      nCopied);
                        else
                            for (long kk = tile[0][0] >= 0 ? tile[0][0] % kDown : -tile[0][0],
                                k = max((tile[0][0] + kDown - 1) / kDown, 0);
                                kk < tileDims[0] && k < dimsl[0];
                                kk += kDown, k++)
                                arraycopy(content, (int) (veclen * ((tileDims[1] * ii + jj) * tileDims[0] + kk)),
                                          target, veclen * ((dimsl[1] * i + j) * dimsl[0] + k),
                                          veclen);
                break;
            case 2:
                for (long jj = tile[1][0] >= 0 ? tile[1][0] % jDown : -tile[1][0],
                    j = max((tile[1][0] + jDown - 1) / jDown, 0);
                    jj < tileDims[1] && j < dimsl[1];
                    jj += jDown, j++)
                    if (kDown == 1)
                        arraycopy(content, (int) (veclen * (jj * tileDims[0] + max(-tile[0][0], 0))),
                                  target, veclen * (j * dimsl[0] + max(tile[0][0], 0)), nCopied);
                    else
                        for (long kk = tile[0][0] >= 0 ? tile[0][0] % kDown : -tile[0][0],
                            k = max((tile[0][0] + kDown - 1) / kDown, 0);
                            kk < tileDims[0] && k < dimsl[0];
                            kk += kDown, k++)
                            arraycopy(content, (int) (veclen * (jj * tileDims[0] + kk)),
                                      target, veclen * (j * dimsl[0] + k),
                                      veclen);
                break;
            case 1:
                if (kDown == 1)
                    arraycopy(content, 0, target, veclen * max(tile[0][0], 0), nCopied);
                else
                    for (long kk = tile[0][0] >= 0 ? tile[0][0] % kDown : -tile[0][0],
                        k = max((tile[0][0] + kDown - 1) / kDown, 0);
                        kk < tileDims[0] && k < dimsl[0];
                        kk += kDown, k++)
                        arraycopy(content, (int) (veclen * kk),
                                  target, veclen * k,
                                  veclen);
                break;

        }
    }

    /**
     * Copies the tile data into the target array.
     *
     * @param tile    tile specification
     *                (low, up, step)i , (low, up, step)j, (low, up, step)k, where low and up are
     *                given in original resolution, up-low+1 is the tile dimension, step is downsize step
     * @param dims    dimensions of the target array
     * @param target  target array
     * @param content content of a tile
     * @param veclen  vector length of the target array
     * @param coord   coordinate
     */
    public static void putTile(int[][] tile, int[] dims, LargeArray target, LargeArray content, int veclen, int coord)
    {
        if (target == null || content == null || target.getType() != content.getType()) {
            throw new IllegalArgumentException("target == null || content == null || target.getType() != content.getType()");
        }
        if (coord == -1) {
            putTile(tile, dims, target, content, veclen);
        } else {
            long tile_size = 1;
            int[] tileDims = new int[dims.length];
            for (int i = 0; i < dims.length; i++) {
                if (tile[i].length < 2 || tile[i][0] > tile[i][1])
                    throw new IllegalArgumentException("tile[i].length < 2 || tile[i][0] > tile[i][1]");
                tileDims[i] = tile[i][1] - tile[i][0] + 1;
                tile_size *= tileDims[i];
            }
            int iDown = 1, jDown = 1, kDown = 1;
            if (tile[0].length > 2)
                kDown = tile[0][2];
            if (tile.length > 1 && tile[1].length > 2)
                jDown = tile[1][2];
            if (tile.length > 2 && tile[2].length > 2)
                iDown = tile[2][2];
            if (content.length() != tile_size)
                throw new IllegalArgumentException("content.length != veclen * k");
            long[] dimsl = new long[dims.length];
            for (int i = 0; i < dimsl.length; i++) {
                dimsl[i] = dims[i];

            }
            switch (dims.length) {
                case 3:
                    for (long ii = tile[2][0] >= 0 ? tile[2][0] % iDown : -tile[2][0],
                        i = max((tile[2][0] + iDown - 1) / iDown, 0);
                        ii < tileDims[2] && i < dimsl[2];
                        ii += iDown, i++)
                        for (long jj = tile[1][0] >= 0 ? tile[1][0] % jDown : -tile[1][0],
                            j = max((tile[1][0] + jDown - 1) / jDown, 0);
                            jj < tileDims[1] && j < dimsl[1];
                            jj += jDown, j++)
                            for (long kk = tile[0][0] >= 0 ? tile[0][0] % kDown : -tile[0][0],
                                k = max((tile[0][0] + kDown - 1) / kDown, 0);
                                kk < tileDims[0] && k < dimsl[0];
                                kk += kDown, k++)
                                target.set(veclen * ((dimsl[1] * i + j) * dimsl[0] + k) + coord,
                                           content.get((tileDims[1] * ii + jj) * tileDims[0] + kk));
                    break;
                case 2:
                    for (long jj = tile[1][0] >= 0 ? tile[1][0] % jDown : -tile[1][0],
                        j = max((tile[1][0] + jDown - 1) / jDown, 0);
                        jj < tileDims[1] && j < dimsl[1];
                        jj += jDown, j++)
                        for (long kk = tile[0][0] >= 0 ? tile[0][0] % kDown : -tile[0][0],
                            k = max((tile[0][0] + kDown - 1) / kDown, 0);
                            kk < tileDims[0] && k < dimsl[0];
                            kk += kDown, k++)
                            target.set(veclen * (j * dimsl[0] + k) + coord, content.get(jj * tileDims[0] + kk));
                    break;
                case 1:
                    for (long kk = tile[0][0] >= 0 ? tile[0][0] % kDown : -tile[0][0],
                        k = max((tile[0][0] + kDown - 1) / kDown, 0);
                        kk < tileDims[0] && k < dimsl[0];
                        kk += kDown, k++)
                        target.set(veclen * k + coord, content.get(kk));
                    break;
            }

        }
    }
}
