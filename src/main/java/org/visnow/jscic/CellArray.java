/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.visnow.jscic.cells.Cell;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.FloatLargeArray;

/**
 * An array that stores cells of the same type.
 *
 * @author Krzysztof S. Nowinski
 *
 * University of Warsaw, ICM
 */
public class CellArray implements java.io.Serializable
{

    private static final long serialVersionUID = 7650812254926446911L;

    private CellType type;
    private int nCells;
    private int nCellNodes;
    private int[] nodes;
    private byte[] orientations;
    private int[] dataIndices;
    private int[] faceIndices;
    private boolean cleanedDuplicates = false;
    private FloatLargeArray cellNormals;
    private float[] cellDihedrals;
    private float[] cellCenters;
    private float[] cellRadii;
    private float[] cellMinCoords;
    private float[] cellMaxCoords;
    private long timestamp;

    /**
     * Creates a new instance of CellArray.
     *
     * @param type cell type
     */
    public CellArray(CellType type)
    {
        this.type = type;
        nCellNodes = type.getNVertices();
        timestamp = System.nanoTime();
    }

    /**
     * Creates a new instance of CellArray.
     *
     * @param type         cell type
     * @param nodes        cell nodes, this reference is used internally (i.e. the
     *                     array is not cloned)
     * @param orientations cell orientations, this reference is used internally
     *                     (i.e. the array is not cloned)
     * @param dataIndices  data indices, this reference is used internally (i.e.
     *                     the array is not cloned)
     */
    public CellArray(CellType type, int[] nodes, byte[] orientations, int[] dataIndices)
    {
        if (nodes.length % type.getNVertices() != 0) {
            throw new IllegalArgumentException("nodes.length % type.getNVertices() != 0");
        }
        if (orientations == null) {
            orientations = new byte[nodes.length / type.getNVertices()];
            for (int i = 0; i < orientations.length; i++) {
                orientations[i] = 1;
            }
        }
        if (nodes.length / type.getNVertices() != orientations.length) {
            throw new IllegalArgumentException("nodes.length / type.getNVertices() != orientations.length");
        }
        this.type = type;
        this.nCellNodes = type.getNVertices();
        this.nCells = orientations.length;
        this.nodes = nodes;
        this.orientations = orientations;
        this.dataIndices = dataIndices;
        this.timestamp = System.nanoTime();
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof CellArray))
            return false;
        CellArray ca = (CellArray) o;
        boolean equal = this.type == ca.type && this.nCells == ca.nCells && this.nCellNodes == ca.nCellNodes && this.cleanedDuplicates == ca.cleanedDuplicates;
        if (equal == false) {
            return false;
        }
        if (this.nodes != null && ca.nodes != null) {
            if (!Arrays.equals(this.nodes, ca.nodes)) return false;
        } else if (this.nodes != ca.nodes) {
            return false;
        }
        if (this.orientations != null && ca.orientations != null) {
            if (!Arrays.equals(this.orientations, ca.orientations)) return false;
        } else if (this.orientations != ca.orientations) {
            return false;
        }
        if (this.dataIndices != null && ca.dataIndices != null) {
            if (!Arrays.equals(this.dataIndices, ca.dataIndices)) return false;
        } else if (this.dataIndices != ca.dataIndices) {
            return false;
        }
        if (this.faceIndices != null && ca.faceIndices != null) {
            if (!Arrays.equals(this.faceIndices, ca.faceIndices)) return false;
        } else if (this.faceIndices != ca.faceIndices) {
            return false;
        }
        if (this.cellNormals != null && ca.cellNormals != null) {
            if (!this.cellNormals.equals(ca.cellNormals)) return false;
        } else if (this.cellNormals != ca.cellNormals) {
            return false;
        }
        if (this.cellDihedrals != null && ca.cellDihedrals != null) {
            if (!Arrays.equals(this.cellDihedrals, ca.cellDihedrals)) return false;
        } else if (this.cellDihedrals != ca.cellDihedrals) {
            return false;
        }
        if (this.cellCenters != null && ca.cellCenters != null) {
            if (!Arrays.equals(this.cellCenters, ca.cellCenters)) return false;
        } else if (this.cellCenters != ca.cellCenters) {
            return false;
        }
        if (this.cellRadii != null && ca.cellRadii != null) {
            if (!Arrays.equals(this.cellRadii, ca.cellRadii)) return false;
        } else if (this.cellRadii != ca.cellRadii) {
            return false;
        }
        if (this.cellMinCoords != null && ca.cellMinCoords != null) {
            if (!Arrays.equals(this.cellMinCoords, ca.cellMinCoords)) return false;
        } else if (this.cellMinCoords != ca.cellMinCoords) {
            return false;
        }
        if (this.cellMaxCoords != null && ca.cellMaxCoords != null) {
            if (!Arrays.equals(this.cellMaxCoords, ca.cellMaxCoords)) return false;
        } else if (this.cellMaxCoords != ca.cellMaxCoords) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        return hashCode(1f);
    }

    /**
     * Generates an approximate hash code using a quality argument. For quality equal to 1 this method is equivalent to hashCode().
     *
     * @param quality a number between 0 and 1.
     *
     * @return an approximate hash code
     */
    public int hashCode(float quality)
    {
        if (quality < 0 || quality > 1) {
            throw new IllegalArgumentException("The quality argument should be between 0 and 1");
        }
        int fprint = 7;
        fprint = 43 * fprint + Objects.hashCode(this.type);
        fprint = 43 * fprint + this.nCells;
        fprint = 43 * fprint + this.nCellNodes;
        fprint = 43 * fprint + Arrays.hashCode(this.nodes);
        fprint = 43 * fprint + Arrays.hashCode(this.orientations);
        fprint = 43 * fprint + Arrays.hashCode(this.dataIndices);
        fprint = 43 * fprint + Arrays.hashCode(this.faceIndices);
        fprint = 43 * fprint + (this.cleanedDuplicates ? 1 : 0);
        fprint = 43 * fprint + (this.cellNormals != null ? this.cellNormals.hashCode(quality) : 0);
        fprint = 43 * fprint + Arrays.hashCode(this.cellDihedrals);
        fprint = 43 * fprint + Arrays.hashCode(this.cellCenters);
        fprint = 43 * fprint + Arrays.hashCode(this.cellRadii);
        fprint = 43 * fprint + Arrays.hashCode(this.cellMinCoords);
        fprint = 43 * fprint + Arrays.hashCode(this.cellMaxCoords);
        return fprint;
    }

    /**
     * Returns the current timestamp.
     *
     * @return timestamp
     */
    public long getTimestamp()
    {
        return timestamp;
    }

    /**
     * Sets the System.nanoTime() as the value of the timestamp
     */
    public void updateTimestamp()
    {
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if the CellArray changed since the give timestamp, false
     * otherwise.
     *
     * @param timestamp timestamp
     *
     * @return true if the CellArray changed since the given timestamp, false
     *         otherwise
     */
    public boolean changedSince(long timestamp)
    {
        return this.timestamp > timestamp;
    }

    /**
     * Returns a shallow copy of this instance.
     *
     * @return a shallow copy of this instance
     */
    public CellArray cloneShallow()
    {
        CellArray clone = new CellArray(type);
        clone.nCells = nCells;
        clone.nodes = nodes;
        clone.orientations = orientations;
        clone.setDataIndices(dataIndices);
        clone.setFaceIndices(faceIndices);
        clone.setCellNormals(cellNormals);
        clone.setCellDihedrals(cellDihedrals);
        clone.setCellCenters(cellCenters);
        clone.setCellRadii(cellRadii);
        clone.setCellMinCoords(cellMinCoords);
        clone.setCellMaxCoords(cellMaxCoords);
        clone.setCleanedDuplicates(cleanedDuplicates);
        clone.timestamp = timestamp;
        return clone;
    }

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public CellArray cloneDeep()
    {
        CellArray clone = new CellArray(type);
        clone.nCells = nCells;
        clone.nodes = nodes != null ? nodes.clone() : null;
        clone.orientations = orientations != null ? orientations.clone() : null;
        clone.setDataIndices(dataIndices != null ? dataIndices.clone() : null);
        clone.setFaceIndices(faceIndices != null ? faceIndices.clone() : null);
        clone.setCellNormals(cellNormals != null ? cellNormals.clone() : null);
        clone.setCellDihedrals(cellDihedrals != null ? cellDihedrals.clone() : null);
        clone.setCellCenters(cellCenters != null ? cellCenters.clone() : null);
        clone.setCellRadii(cellRadii != null ? cellRadii.clone() : null);
        clone.setCellMinCoords(cellMinCoords != null ? cellMinCoords.clone() : null);
        clone.setCellMaxCoords(cellMaxCoords != null ? cellMaxCoords.clone() : null);
        clone.setCleanedDuplicates(cleanedDuplicates);
        clone.timestamp = timestamp;
        return clone;
    }

    /**
     * Returns the cell type
     *
     * @return cell type
     */
    public CellType getType()
    {
        return type;
    }

    /**
     * Returns the dimension of the cells.
     *
     * @return dimension of the cells
     */
    public int getDim()
    {
        return type.getDim();
    }

    /**
     * Returns the number of cells.
     *
     * @return number of cells
     */
    public int getNCells()
    {
        return nCells;
    }

    /**
     * Returns the number of vertices of each cell in this array.
     *
     * @return number of vertices of each cell
     */
    public int getNCellNodes()
    {
        return nCellNodes;
    }

    /**
     * Returns the cell nodes.
     *
     * @return cell nodes
     */
    public int[] getNodes()
    {
        return nodes;
    }

    /**
     * Returns the nodes of the specified cell.
     *
     * @param ind cell index
     *
     * @return nodes of the specified cell
     */
    public int[] getNodes(int ind)
    {
        int n = type.getNVertices();
        int[] selNodes = new int[n];
        for (int i = 0; i < n; i++) {
            selNodes[i] = nodes[ind * n + i];
        }
        return selNodes;
    }

    /**
     * Sets the nodes of the specified cell.
     *
     * @param ind      cell index
     * @param newNodes new value of nodes of the cell, this reference is used
     *                 internally (i.e. the array is not cloned)
     */
    public void setNodes(int ind, int[] newNodes)
    {
        if (newNodes == null || newNodes.length != type.getNVertices()) {
            throw new IllegalArgumentException("newNodes.length != type.getNVertices()");
        }
        int n = type.getNVertices();
        System.arraycopy(newNodes, 0, nodes, ind * n, n);
        cleanedDuplicates = false;
        timestamp = System.nanoTime();
    }

    /**
     * Returns cell orientations.
     *
     * @return cell orientations
     */
    public byte[] getOrientations()
    {
        return orientations;
    }

    /**
     * Returns the orientation at specified index.
     *
     * @param index cell index
     *
     * @return orientation at specified index
     */
    public byte getOrientation(int index)
    {
        return this.orientations[index];
    }

    /**
     * Sets the orientation at specified index.
     *
     * @param index          index
     * @param newOrientation new value of orientation at specified index
     */
    public void setOrientation(int index, byte newOrientation)
    {
        this.orientations[index] = newOrientation == 0 ? (byte) 0 : (byte) 1;
        timestamp = System.nanoTime();
    }

    /**
     * Sets the cell nodes and orientations.
     *
     * @param nodes        cell nodes, this reference is used internally (i.e. the
     *                     array is not cloned)
     * @param orientations cell orientations, this reference is used internally
     *                     (i.e. the array is not cloned)
     */
    public void setData(int[] nodes, byte[] orientations)
    {
        if (nodes.length % type.getNVertices() != 0 || nodes.length / type.getNVertices() != orientations.length) {
            throw new IllegalArgumentException("nodes.length % type.getNVertices() != 0 || nodes.length / type.getNVertices() != orientations.length");
        }
        this.nCells = orientations.length;
        this.nodes = nodes;
        this.orientations = orientations;
        this.timestamp = System.nanoTime();
    }

    /**
     * Returns data indices.
     *
     * @return data indices
     */
    public int[] getDataIndices()
    {
        return dataIndices;
    }

    /**
     * Sets the data indices.
     *
     * @param dataIndices new value of data indices, this reference is used
     *                    internally (i.e. the array is not cloned)
     */
    public void setDataIndices(int[] dataIndices)
    {
        this.dataIndices = dataIndices;
        this.timestamp = System.nanoTime();
    }

    /**
     * Returns the data index for the specified cell index.
     *
     * @param ind - cell index
     *
     * @return the value of data index
     */
    public int getDataIndices(int ind)
    {
        return dataIndices[ind];
    }

    /**
     * Sets the data index for the specified cell index.
     *
     * @param ind         cell index
     * @param dataIndices new value of data index
     */
    public void setDataIndices(int ind, int dataIndices)
    {
        this.dataIndices[ind] = dataIndices;
        timestamp = System.nanoTime();
    }

    /**
     * Returns cell normals.
     *
     * @return cell normals
     */
    public FloatLargeArray getCellNormals()
    {
        return cellNormals;
    }

    /**
     * Sets cell normals.
     *
     * @param cellNormals new value of cell normals, this reference is used
     *                    internally (i.e. the array is not cloned)
     */
    public void setCellNormals(FloatLargeArray cellNormals)
    {
        this.cellNormals = cellNormals;
        timestamp = System.nanoTime();
    }

    /**
     * Sets cell centers.
     *
     * @param cellCenters cell centers, this reference is used internally (i.e.
     *                    the array is not cloned)
     */
    public void setCellCenters(float[] cellCenters)
    {
        this.cellCenters = cellCenters;
        timestamp = System.nanoTime();
    }

    /**
     * Sets cell radii.
     *
     * @param cellRadii cell radii, this reference is used internally (i.e. the
     *                  array is not cloned)
     */
    public void setCellRadii(float[] cellRadii)
    {
        this.cellRadii = cellRadii;
        timestamp = System.nanoTime();
    }

    /**
     * Sets the minimum values of coorinates for each cell.
     *
     * @param cellMinCoords new minimum values of coorinates for each cell
     */
    public void setCellMinCoords(float[] cellMinCoords)
    {
        if (cellMinCoords != null && cellMinCoords.length != 0 && cellMinCoords.length != 3 * nCells) {
            throw new IllegalArgumentException("cellMinCoords != null && cellMinCoords.length != 0 && cellMinCoords.length != 3 * nCells");
        }
        this.cellMinCoords = cellMinCoords;
        timestamp = System.nanoTime();
    }

    /**
     * Sets the maximum values of coorinates for each cell.
     *
     * @param cellMaxCoords new maximum values of coorinates for each cell
     */
    public void setCellMaxCoords(float[] cellMaxCoords)
    {
        if (cellMaxCoords != null && cellMaxCoords.length != 0 && cellMaxCoords.length != 3 * nCells) {
            throw new IllegalArgumentException("cellMaxCoords != null &&  cellMaxCoords.length != 0 && cellMaxCoords.length != 3 * nCells");
        }
        this.cellMaxCoords = cellMaxCoords;
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if this cell array contains no pairs of cells with the same
     * nodes and opposite orientations, false if this condition is not checked.
     *
     * @return true if this cell array contains no pairs of cells with the same
     *         nodes and opposite orientations, false if this condition is not checked
     */
    public boolean isCleanedDuplicates()
    {
        return cleanedDuplicates;
    }

    /**
     * Sets the new value of cleanedDuplicates property.
     *
     * @param cleanedDuplicates the new value of cleanedDuplicates property.
     */
    public void setCleanedDuplicates(boolean cleanedDuplicates)
    {
        this.cleanedDuplicates = cleanedDuplicates;
        timestamp = System.nanoTime();
    }

    /**
     * Compare two CellArray objects. Returns true if both of them have the same
     * nodes, false otherwise.
     *
     * @param ca input cell array
     *
     * @return true if both CellArrays have the same nodes, false otherwise
     */
    public boolean isStructCompatible(CellArray ca)
    {
        if (ca == null || ca.getType() != type) {
            return false;
        }
        if (ca.getTimestamp() == getTimestamp()) {
            return true;
        }
        int[] caNodes = ca.getNodes();
        if (caNodes == nodes) {
            return true;
        }
        if (caNodes.length != nodes.length) {
            return false;
        }
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] != caNodes[i]) {
                return false;
            }
        }
        return true;
    }

    private int shortHash(int k)
    {
        short seed = 13131; // 31 131 1313 13131 131313 etc..
        short h = 0;

        for (int i = 0; i < nCellNodes; i++) {
            h = (short) (h * seed + nodes[nCellNodes * k + i]);
        }
        return (int) h - Short.MIN_VALUE;
    }

    /**
     * Removes cells occurring twice (usually, internal cell faces).
     */
    public void cancelDuplicates()
    {
        if (cleanedDuplicates) {
            return;
        }
        int startBinSize = 2;
        int[][] indexBins = new int[65536][startBinSize];
        int[] indexN = new int[65536];
        int n = nCells;
        for (int i = 0; i < indexN.length; i++) {
            indexN[i] = 0;
        }

        for (int i = 0; i < nCells; i++) {
            int h = shortHash(i);
            int k = -1;
            s1:
            for (int j = 0; j < indexN[h]; j++) {
                int l = indexBins[h][j];
                for (int m = 0; m < nCellNodes; m++) {
                    if (nodes[nCellNodes * i + m] != nodes[nCellNodes * l + m]) {
                        continue s1; // cell i is not a duplicate of cell j in bin h
                    }
                }
                k = j; // all nodes match - duplicate found
                break;
            }
            if (k != -1) // duplicate found - removing
            {
                for (int j = k + 1; j < indexN[h]; j++) {
                    indexBins[h][j - 1] = indexBins[h][j];
                }
                indexN[h] -= 1;
                n -= 2; // two cells removed
            } else {
                if (indexN[h] + 1 >= indexBins[h].length) // increase bin capacity
                {
                    int[] t = new int[2 * indexBins[h].length];
                    System.arraycopy(indexBins[h], 0, t, 0, indexBins[h].length);
                    indexBins[h] = t;
                }
                indexBins[h][indexN[h]] = i;
                indexN[h] += 1; // cell index added to bin
            }
        }

        int maxBin = 0;
        for (int i = 0; i < indexN.length; i++) {
            if (maxBin < indexN[i]) {
                maxBin = indexN[i];
            }
        }
        int[] xNodes = new int[nCellNodes * n];
        byte[] xOrientations = new byte[n];
        if (dataIndices != null) {
            int[] xDataIndices = new int[n];
            for (int i = 0, l = 0; i < 65536; i++) {
                for (int j = 0; j < indexN[i]; j++, l++) {
                    xDataIndices[l] = dataIndices[indexBins[i][j]];
                }
            }
            dataIndices = xDataIndices;
        }
        for (int i = 0, l = 0; i < 65536; i++) {
            for (int j = 0; j < indexN[i]; j++, l++) {
                int m = indexBins[i][j];
                for (int k = 0; k < nCellNodes; k++) {
                    xNodes[l * nCellNodes + k] = nodes[m * nCellNodes + k];
                }
                xOrientations[l] = orientations[m];
            }
        }
        nCells = n;
        nodes = xNodes;
        orientations = xOrientations;
        cleanedDuplicates = true;
        timestamp = System.nanoTime();
    }

    /**
     * Generates the vector of arrays containing all faces of the cells.
     *
     * @return list of cell arrays containing all faces of all cells of the
     *         array
     */
    ArrayList<CellArray> getFaces()
    {
        CellType[] cellTypes = Cell.getProperCellTypes();
        Cell[] stdCells = new Cell[cellTypes.length];
        for (int i = 0; i < stdCells.length; i++) {
            stdCells[i] = Cell.createCell(cellTypes[i], new int[cellTypes[i].getNVertices()], (byte) 1);
        }
        CellArray[] fcs = new CellArray[8];
        int[][] v = new int[8][];
        byte[][] or = new byte[8][];
        int[][] in = new int[8][];
        int[][] fcsOf = new int[8][];
        int[] faceTypes = Cell.cellTypeToFaceType(type);
        for (int i = 0; i < faceTypes.length; i++) {
            if (faceTypes[i] != 0) {
                int[] caNodes = new int[nCells * faceTypes[i] * CellType.getType(i).getNVertices()];
                byte[] caOrientations = new byte[nCells * faceTypes[i]];
                int[] caIndices = null;
                if (dataIndices != null) {
                    caIndices = new int[nCells * faceTypes[i]];
                    for (int j = 0; j < caIndices.length; j++) {
                        caIndices[j] = dataIndices[j / faceTypes[i]];
                    }
                }
                int[] caFcsOf = new int[nCells * faceTypes[i]];
                fcs[i] = new CellArray(CellType.getType(i), caNodes, caOrientations, caIndices);
                v[i] = caNodes;
                or[i] = caOrientations;
                in[i] = caIndices;
                fcsOf[i] = caFcsOf;
            } else {
                fcs[i] = null;
            }
        }
        int nv = type.getNVertices();
        int[] cv = new int[nv];
        int[] ci = new int[8];
        for (int i = 0; i < 8; i++) {
            ci[i] = 0;
        }
        for (int i = 0; i < nCells; i++) {
            for (int j = 0; j < nv; j++) {
                cv[j] = nodes[nv * i + j];
            }
            Cell[] faces = stdCells[type.getValue()].faces(cv, orientations[i]);
            for (int j = 0; j < faces.length; j++) {
                Cell cell = faces[j];
                int k = cell.getType().getValue();
                int l = ci[k];
                or[k][l] = cell.getOrientation();
                if (dataIndices != null) {
                    in[k][l] = dataIndices[i];
                }
                fcsOf[k][l] = i; //parent cell
                int nfv = CellType.getType(k).getNVertices();
                System.arraycopy(cell.getVertices(), 0, v[k], l * nfv, nfv);
                ci[k] += 1;
            }
        }
        ArrayList<CellArray> faces = new ArrayList<>();
        for (int i = 0; i < fcs.length; i++) {
            if (fcs[i] != null) {
                faces.add(fcs[i]);
                CellArray[] arFcsOf = new CellArray[fcsOf[i].length];
                for (int j = 0; j < arFcsOf.length; j++) {
                    arFcsOf[j] = this;
                }
            }
        }
        return faces;
    }

    /**
     * Returns cell dihedrals. For edges on the surface, dihedral of an edge is
     * the angle between normals to cell containg this edge.
     *
     * @return cell dihedrals
     */
    public float[] getCellDihedrals()
    {
        return cellDihedrals;
    }

    /**
     * Sets cell dihedrals.
     *
     * @param cellDihedrals new value of cell dihedrals, this reference is used
     *                      internally (i.e. the array is not cloned)
     */
    public void setCellDihedrals(float[] cellDihedrals)
    {
        this.cellDihedrals = cellDihedrals;
        timestamp = System.nanoTime();
    }

    /**
     * Returns face indices.
     *
     * @return face indices
     */
    public int[] getFaceIndices()
    {
        return faceIndices;
    }

    /**
     * Sets face indices.
     *
     * @param faceIndices new value of face indices, this reference is used
     *                    internally (i.e. the array is not cloned)
     */
    void setFaceIndices(int[] faceIndices)
    {
        this.faceIndices = faceIndices;
        timestamp = System.nanoTime();
    }

    /**
     * Returns CellArray containing cell edges.
     *
     * @return cell edges
     */
    public CellArray getEdges()
    {
        int nEdges = Cell.cellTypeToFaceType(type)[1];
        if (nEdges == 0) {
            return null;
        }
        int[] caNodes = new int[nCells * nEdges * CellType.getType(1).getNVertices()];
        byte[] caOrientations = new byte[nCells * nEdges];
        int[] caIndices = new int[nCells * nEdges];
        int[] fcsOf = new int[nCells * nEdges];
        CellArray edges = new CellArray(CellType.SEGMENT, caNodes, caOrientations, caIndices);
        int nv = type.getNVertices();
        int[] cv = new int[nv];
        for (int i = 0, l = 0; i < nCells; i++) {
            for (int j = 0; j < nv; j++) {
                cv[j] = nodes[nv * i + j];
            }
            Cell[] faces = Cell.createCell(type, 3, cv, orientations[i]).faces();
            for (int j = 0; j < faces.length; j++) {
                Cell cell = faces[j];
                if (cell.getType() != CellType.SEGMENT) {
                    continue;
                }
                caOrientations[l] = cell.getOrientation();
                if (dataIndices != null) {
                    caIndices[l] = dataIndices[i]; //faces inherit data from original cell
                }
                fcsOf[l] = i;
                int nfv = CellType.getType(1).getNVertices();
                System.arraycopy(cell.getVertices(), 0, caNodes, l * nfv, nfv);
                l += 1;
            }
        }
        edges.setFaceIndices(fcsOf);
        return edges;
    }

    /**
     * Returns cell vertices for the specified cell index.
     *
     * @param i cell index
     *
     * @return cell vertices for the specified cell index
     */
    public int[] getCellVertices(int i)
    {
        if (i < 0 || i >= nCells) {
            throw new IllegalArgumentException("i < 0 || i >= nCells");
        }
        int[] cellVerts = new int[nCellNodes];
        System.arraycopy(nodes, i * nCellNodes, cellVerts, 0, nCellNodes);
        return cellVerts;
    }

    /**
     * Returns cell at specified index.
     *
     * @param i cell index
     *
     * @return cell at specified index
     */
    public Cell getCell(int i)
    {
        if (i < 0 || i >= nCells) {
            throw new IllegalArgumentException("i < 0 || i >= nCells");
        }
        int[] cellVerts = new int[nCellNodes];
        System.arraycopy(nodes, i * nCellNodes, cellVerts, 0, nCellNodes);
        return Cell.createCell(type, cellVerts, orientations[i]);
    }

    /**
     * Triangulates this cell array.
     *
     * @return cell arrays of simplices
     */
    public CellArray getTriangulated()
    {
        int nv = type.getNVertices();
        int mult = 1;
        CellType simplex = CellType.POINT;
        switch (this.type) {
            case POINT:
                return this;
            case SEGMENT:
                return this;
            case TRIANGLE:
                return this;
            case TETRA:
                return this;
            case QUAD:
                simplex = CellType.TRIANGLE;
                mult = 2;
                break;
            case PYRAMID:
                simplex = CellType.TETRA;
                mult = 2;
                break;
            case PRISM:
                simplex = CellType.TETRA;
                mult = 3;
                break;
            case HEXAHEDRON:
                simplex = CellType.TETRA;
                mult = 6;
                break;
            default:
                simplex = CellType.TETRA;
                mult = 3;
                break;
        }
        int[] trNodes = new int[mult * simplex.getNVertices() * nCells];
        byte[] trOrientations = new byte[mult * nCells];
        int[] cellVerts = new int[nv];
        int ntv = simplex.getNVertices();
        for (int i = 0, l = 0, n = 0; i < nCells; i++) {
            for (int j = 0; j < nv; j++) {
                cellVerts[j] = nodes[i * nv + j];
            }
            Cell[] tr = Cell.createCell(type, cellVerts, orientations[i]).triangulation();
            for (int j = 0; j < tr.length; j++, n++) {
                for (int k = 0; k < ntv; k++, l++) {
                    trNodes[l] = tr[j].getVertices()[k];
                }
                trOrientations[n] = tr[j].getOrientation();
            }
        }
        if (dataIndices != null && dataIndices.length == nCells) {
            int[] trDataIndices = new int[mult * nCells];
            for (int i = 0; i < trDataIndices.length; i++) {
                trDataIndices[i] = dataIndices[i / mult];
            }
            return new CellArray(simplex, trNodes, trOrientations, trDataIndices);
        }
        return new CellArray(simplex, trNodes, trOrientations, null);
    }

    /**
     * Prints the content of this CellArray.
     */
    public void printContent()
    {
        System.out.println("" + nCells + " " + type.getUCDName());
        int n = type.getNVertices();
        for (int i = 0; i < nCells; i++) {
            for (int j = 0; j < n; j++) {
                System.out.printf("%4d ", nodes[n * i + j]);
            }
            System.out.println("" + orientations[i]);
        }
    }

    /**
     * Returns the measure (length, area, volume) of a polyhedron described by
     * this cell array.
     *
     * @param coords coordinates
     *
     * @return the measure of this cell array
     */
    public float getMeasure(FloatLargeArray coords)
    {
        float r = 0;
        for (int i = 0; i < nCells; i++) {
            int[] cellVerts = new int[nCellNodes];
            System.arraycopy(nodes, i * nCellNodes, cellVerts, 0, nCellNodes);
            r += Cell.createCell(type, cellVerts, orientations[i]).getMeasure(coords);
        }
        return r;
    }

    /**
     * Returns cell centers.
     *
     * @return cell centers
     */
    public float[] getCellCenters()
    {
        return cellCenters;
    }

    /**
     * Returns cell radii.
     *
     * @return cell radii
     */
    public float[] getCellRadii()
    {
        return cellRadii;
    }

    /**
     * Returns true if this cell array is of type simplex, false otherwise.
     *
     * @return true if this cell array is of type simplex, false otherwise
     */
    public boolean isSimplicesArray()
    {
        return (type == CellType.TETRA || type == CellType.TRIANGLE || type == CellType.SEGMENT || type == CellType.POINT);

    }

    private class ComputeGeometryData implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;
        FloatLargeArray coords;

        public ComputeGeometryData(int nThreads, int iThread, FloatLargeArray coords)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.coords = coords;
        }

        @Override
        public void run()
        {

            for (int k = iThread; k < nCells; k += nThreads) {
                float[] c = new float[3];
                for (int l = 0; l < 3; l++) {
                    c[l] = 0;
                }
                for (int l = 0; l < nCellNodes; l++) {
                    long cStart = 3 * (long) nodes[k * nCellNodes + l];
                    for (int m = 0; m < 3; m++) {
                        c[m] += coords.getFloat(cStart + m);
                    }
                }
                for (int l = 0; l < c.length; l++) {
                    c[l] /= nCellNodes;
                }
                float r = 0;
                for (int l = 0; l < nCellNodes; l++) {
                    long cStart = 3 * (long) nodes[k * nCellNodes + l];
                    for (int m = 0; m < 3; m++) {
                        r += (c[m] - coords.getFloat(cStart + m)) * (c[m] - coords.getFloat(cStart + m));
                    }
                }
                System.arraycopy(c, 0, cellCenters, 3 * k, 3);
                cellRadii[k] = (float) sqrt(r);
            }
        }
    }

    private class ComputeCellNormals implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;
        FloatLargeArray coords;

        public ComputeCellNormals(int nThreads, int iThread, FloatLargeArray coords)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.coords = coords;
        }

        @Override
        public void run()
        {
            float[] v0 = new float[3];
            float[] v1 = new float[3];

            for (int i = iThread; i < nCells; i += nThreads) {
                for (int j = 0; j < 3; j++) {
                    v0[j] = coords.getFloat(3 * (long) nodes[i * nCellNodes + 1] + j) - coords.getFloat(3 * (long) nodes[i * nCellNodes] + j);
                    v1[j] = coords.getFloat(3 * (long) nodes[i * nCellNodes + 2] + j) - coords.getFloat(3 * (long) nodes[i * nCellNodes] + j);
                }
                cellNormals.setFloat(3 * (long) i, v0[1] * v1[2] - v0[2] * v1[1]);
                cellNormals.setFloat(3 * (long) i + 1, v0[2] * v1[0] - v0[0] * v1[2]);
                cellNormals.setFloat(3 * (long) i + 2, v0[0] * v1[1] - v0[1] * v1[0]);
                float r = cellNormals.getFloat(3 * (long) i) * cellNormals.getFloat(3 * (long) i) +
                    cellNormals.getFloat(3 * (long) i + 1) * cellNormals.getFloat(3 * (long) i + 1) +
                    cellNormals.getFloat(3 * (long) i + 2) * cellNormals.getFloat(3 * (long) i + 2);
                r = (float) (sqrt(r));
                if (r < .000001) {
                    r = .000001f;
                }
                if (orientations[i] == 0) {
                    r = -r;
                }
                for (int j = 0; j < 3; j++) {
                    cellNormals.setFloat(3 * (long) i + j, cellNormals.getFloat(3 * (long) i + j) / r);
                }
            }
        }
    }

    /**
     * Computes cell centers and radii.
     *
     * @param coords coordinates
     */
    public void computeCellGeometryData(FloatLargeArray coords)
    {
        cellRadii = new float[nCells];
        cellCenters = new float[3 * nCells];
        int nThreads = ConcurrencyUtils.getNumberOfThreads();
        Future[] futures = new Future[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            futures[iThread] = ConcurrencyUtils.submit(new ComputeGeometryData(nThreads, iThread, coords));
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException();
        }
        timestamp = System.nanoTime();
    }

    /**
     * Computes normal vectors for 2D cells.
     *
     * @param coords coordinates
     */
    public void computeCellNormals(FloatLargeArray coords)
    {
        if (nCells == 0) {
            return;
        }
        cellNormals = new FloatLargeArray((long) 3 * (long) nCells);
        int nThreads = ConcurrencyUtils.getNumberOfThreads();
        Future[] futures = new Future[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            futures[iThread] = ConcurrencyUtils.submit(new ComputeCellNormals(nThreads, iThread, coords));
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException();
        }
    }

    /**
     * Returns the minimum values of coordinates for each cell.
     *
     * @return minimum values of coordinates for each cell
     */
    public float[] getCellMinCoords()
    {
        return cellMinCoords;
    }

    /**
     * Returns the maximum values of coordinates for each cell.
     *
     * @return maximum values of coordinates for each cell
     */
    public float[] getCellMaxCoords()
    {
        return cellMaxCoords;
    }

}
